package util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import controller.MainApp;
import model.Appareil;
import model.Capteur;
import model.CasqueAudio;
import model.CasqueVR;
import model.Emprunt;
import model.Institution;
import model.Lieu;
import model.ManetteCasque;
import model.ManetteJeu;
import model.Materiel;
import model.PeripheriqueEntree;
import model.PeripheriqueSortie;
import model.Souris;
import model.Stock;
import model.Tablette;
import model.Telephone;
import model.Utilisateur;
import model.Webcam;

/**
 * Cette classe est utilisée pour écrire dans les fichier de données XML
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Writter {

	/**
	 * Cette méthode permet de sauvegarder les emprunts dans un fichier XML
	 * 
	 * @param liste La liste des emprunts
	 * @param utilisateurs La liste des utilisateurs
	 * @param materiels La liste des materiels
	 */
	public static void emprunt(List<Emprunt> liste, List<Utilisateur> utilisateurs, List<Materiel> materiels) {

		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("emprunts");
			doc.appendChild(rootElement);

			for (Emprunt emprunt : liste) {
				// Emprunt elements
				Element e = doc.createElement("emprunt");
				rootElement.appendChild(e);
				
				Element utilisateur = doc.createElement("utilisateur");
				utilisateur.appendChild(doc.createTextNode(Integer.toString(emprunt.getUtilisateur().getId())));
				e.appendChild(utilisateur);
				
				Element materiel = doc.createElement("materiel");
				materiel.appendChild(doc.createTextNode(Integer.toString(emprunt.getMat().getId())));
				e.appendChild(materiel);
				
				Element raison = doc.createElement("raison");
				raison.appendChild(doc.createTextNode(emprunt.getRaison()));
				e.appendChild(raison);
				
				String date = "";
				if (emprunt.getDateEmprunt() != null) date = new SimpleDateFormat("yyyy-MM-dd").format(emprunt.getDateEmprunt());
				Element dateDepart = doc.createElement("dateDepart");
				dateDepart.appendChild(doc.createTextNode(date));
				e.appendChild(dateDepart);
				
				date = "";
				if (emprunt.getDateRetour() != null) date = new SimpleDateFormat("yyyy-MM-dd").format(emprunt.getDateRetour());
				Element dateFin = doc.createElement("dateFin");
				dateFin.appendChild(doc.createTextNode(date));
				e.appendChild(dateFin);
			}
	
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(MainApp.class.getClassLoader().getResource("sauvegardes/emprunts.xml").getPath()));
	
			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
	
			transformer.transform(source, result);

			System.out.println("Emprunts sauvegardés !");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	
	/**
	 * Cette méthode permet de sauvegarder les materiels dans un fichier XML
	 * 
	 * @param liste La liste des materiels
	 */
	public static void materiel(List<Materiel> liste) {
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("materiels");
			doc.appendChild(rootElement);
			
			for (Materiel mat : liste) {
				Element e = doc.createElement("materiel");
				
				if (mat instanceof Capteur) e = doc.createElement("Capteur");
	    		if (mat instanceof CasqueAudio) e = doc.createElement("CasqueAudio");
	    		if (mat instanceof CasqueVR) e = doc.createElement("CasqueVR");
	    		if (mat instanceof ManetteCasque) e = doc.createElement("ManetteCasque");
	    		if (mat instanceof ManetteJeu) e = doc.createElement("ManetteJeu");
	    		if (mat instanceof Souris) e = doc.createElement("Souris");
	    		if (mat instanceof Tablette) e = doc.createElement("Tablette");
	    		if (mat instanceof Telephone) e = doc.createElement("Telephone");
	    		if (mat instanceof Webcam) e = doc.createElement("Webcam");
				
				rootElement.appendChild(e);
				
				Element id = doc.createElement("id");
				id.appendChild(doc.createTextNode(Integer.toString(mat.getId())));
				e.appendChild(id);
				
				
				Element ref = doc.createElement("ref");
				ref.appendChild(doc.createTextNode(mat.getRef()));
				e.appendChild(ref);
				
				Element nom = doc.createElement("nom");
				nom.appendChild(doc.createTextNode(mat.getNom()));
				e.appendChild(nom);
				
				Element marque = doc.createElement("marque");
				marque.appendChild(doc.createTextNode(mat.getMarque()));
				e.appendChild(marque);
				
				Element prix = doc.createElement("prix");
				prix.appendChild(doc.createTextNode(Double.toString(mat.getPrix())));
				e.appendChild(prix);
				
				String date = "";
				if (mat.getEtat() != null) date = mat.getEtat().toString();
				Element etat = doc.createElement("etat");
				etat.appendChild(doc.createTextNode(date));
				e.appendChild(etat);
				
				date = "";
				if (mat.getDateAchat() != null) date = new SimpleDateFormat("yyyy-MM-dd").format(mat.getDateAchat());
				
				Element dateAchat = doc.createElement("dateAchat");
				dateAchat.appendChild(doc.createTextNode(date));
				e.appendChild(dateAchat);
				
				String ID = "-1";
				if (mat.getInsti() != null) ID = Integer.toString(mat.getInsti().getId());
				Element instiId = doc.createElement("instiId");
				instiId.appendChild(doc.createTextNode(ID));
				e.appendChild(instiId);
				
				ID = "-1";
				if (mat.getStock() != null) ID = Integer.toString(mat.getStock().getId());
				Element stockId = doc.createElement("stockId");
				stockId.appendChild(doc.createTextNode(ID));
				e.appendChild(stockId);
				
				if (mat instanceof Appareil) {
					date = "";
					if (((Appareil) mat).getOs() != null) date = ((Appareil) mat).getOs().toString();
					Element os = doc.createElement("os");
					os.appendChild(doc.createTextNode(date));
					e.appendChild(os);
					
					date = "";
					if (((Appareil) mat).getTaille() != null) date = Double.toString(((Appareil) mat).getTaille());
					Element taille = doc.createElement("taille");
					taille.appendChild(doc.createTextNode(date));
					e.appendChild(taille);
					
					date = "";
					if (((Appareil) mat).getResolution() != null) date = ((Appareil) mat).getResolution().toString();
					Element resolution = doc.createElement("resolution");
					resolution.appendChild(doc.createTextNode(date));
					e.appendChild(resolution);
					
				} else if (mat instanceof PeripheriqueEntree) {
					date = "";
					date = Integer.toString(((PeripheriqueEntree) mat).getSensibilite());
					Element sensibilite = doc.createElement("sensibilite");
					sensibilite.appendChild(doc.createTextNode(date));
					e.appendChild(sensibilite);
					
					date = "";
					if (((PeripheriqueEntree) mat).getConnectique() != null) date = ((PeripheriqueEntree) mat).getConnectique().toString();
					Element connectique = doc.createElement("connectique");
					connectique.appendChild(doc.createTextNode(date));
					e.appendChild(connectique);
					
				} else if (mat instanceof PeripheriqueSortie) {
					date = "";
					if (((PeripheriqueSortie) mat).getQualite() != null) date = ((PeripheriqueSortie) mat).getQualite().toString();
					Element qualite = doc.createElement("qualite");
					qualite.appendChild(doc.createTextNode(date));
					e.appendChild(qualite);
				}
			}
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(MainApp.class.getClassLoader().getResource("sauvegardes/materiels.xml").getPath()));
			
			// Output to console for testing
			//StreamResult result = new StreamResult(System.out);
			
			transformer.transform(source, result);

			System.out.println("Materiels sauvegardés !");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	
	/**
	 * Cette méthode permet de sauvegarder les institutions, les lieux et les stocks
	 * 
	 * @param stocks La liste des stocks
	 * @param lieux La liste des lieux
	 * @param institutions La liste des institutions
	 */
	public static void emplacement(List<Stock> stocks, List<Lieu> lieux, List<Institution> institutions) {
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("emplacements");
			doc.appendChild(rootElement);
			
			
			for (Institution inst : institutions) {
				Element e = doc.createElement("institution");
				
				rootElement.appendChild(e);
				
				Element id = doc.createElement("id");
				id.appendChild(doc.createTextNode(Integer.toString(inst.getId())));
				e.appendChild(id);
				
				
				Element name = doc.createElement("name");
				name.appendChild(doc.createTextNode(inst.getName()));
				e.appendChild(name);
				
				Element adresse = doc.createElement("adresse");
				adresse.appendChild(doc.createTextNode(inst.getAdresse()));
				e.appendChild(adresse);
				
			}
			for (Lieu l : lieux) {
				Element e = doc.createElement("lieu");
				
				rootElement.appendChild(e);
				
				Element id = doc.createElement("id");
				id.appendChild(doc.createTextNode(Integer.toString(l.getId())));
				e.appendChild(id);
				
				
				Element nom_lieu = doc.createElement("nom_lieu");
				nom_lieu.appendChild(doc.createTextNode(l.getNom_lieu()));
				e.appendChild(nom_lieu);
				
				String ID = "-1";
				if (l.getInsti() != null) ID = Integer.toString(l.getInsti().getId());
				Element instiId = doc.createElement("instiId");
				instiId.appendChild(doc.createTextNode(ID));
				e.appendChild(instiId);
				
				ID = "-1";
				if (l.getRespo() != null) ID = Integer.toString(l.getRespo().getId());
				Element respoId = doc.createElement("respoId");
				respoId.appendChild(doc.createTextNode(ID));
				e.appendChild(respoId);
			}
			for (Stock s : stocks) {
				Element e = doc.createElement("stock");
				
				rootElement.appendChild(e);
				
				Element id = doc.createElement("id");
				id.appendChild(doc.createTextNode(Integer.toString(s.getId())));
				e.appendChild(id);
				
				Element id_stock = doc.createElement("id_stock");
				id_stock.appendChild(doc.createTextNode(Integer.toString(s.getId_stock())));
				e.appendChild(id_stock);
				
				String ID = "-1";
				if (s.getLieu() != null) ID = Integer.toString(s.getLieu().getId());
				Element lieuId = doc.createElement("lieuId");
				lieuId.appendChild(doc.createTextNode(ID));
				e.appendChild(lieuId);
			}
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(MainApp.class.getClassLoader().getResource("sauvegardes/emplacements.xml").getPath()));
			
			// Output to console for testing
			//StreamResult result = new StreamResult(System.out);
			
			transformer.transform(source, result);

			System.out.println("Emplacements sauvegardés !");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
}