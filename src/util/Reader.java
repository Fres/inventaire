package util;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import controller.MainApp;
import model.*;
import model.Appareil.OS;
import model.Appareil.Res;
import model.PeripheriqueEntree.connect;
import model.PeripheriqueSortie.QUALITE;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Cette classe est utilisée pour lire les fichier de données XML
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Reader {
	/**
	 * Cette méthode permet de récupérer les emprunts
	 * 
	 * @param liste La liste des emprunts
	 * @param utilisateurs La liste des utilisateurs
	 * @param materiels La liste des materiels
	 */
	  public static void emprunt(List<Emprunt> liste, List<Utilisateur> utilisateurs, List<Materiel> materiels) {
		  liste.clear();
		  try {
	
			  File fXmlFile = new File(MainApp.class.getClassLoader().getResource("sauvegardes/emprunts.xml").getPath());
			  DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			  DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			  Document doc = dBuilder.parse(fXmlFile);
						
			  //optional, but recommended
			  //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			  doc.getDocumentElement().normalize();
						
			  NodeList nList = doc.getElementsByTagName("emprunt");
	
			  for (int temp = 0; temp < nList.getLength(); temp++) {
				  Node nNode = nList.item(temp);
							
				  if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			
					  Element e = (Element) nNode;
					  
					  int utilisateurId = Integer.parseInt(e.getElementsByTagName("utilisateur").item(0).getTextContent());
					  int materielId = Integer.parseInt(e.getElementsByTagName("materiel").item(0).getTextContent());
					  String raison = e.getElementsByTagName("raison").item(0).getTextContent();
					  Date dateDepart = new SimpleDateFormat("yyyy-MM-dd").parse(e.getElementsByTagName("dateDepart").item(0).getTextContent());
					  Date dateFin = new SimpleDateFormat("yyyy-MM-dd").parse(e.getElementsByTagName("dateFin").item(0).getTextContent());
					  
					  Utilisateur u = null;
					  Materiel m = null;
					  
					  for (Utilisateur user : utilisateurs) {
						  if (utilisateurId == user.getId()) {
							  u = user;
							  break;
						  }
					  }
					  for (Materiel mat : materiels) {
						  if (materielId == mat.getId()) {
							  m = mat;
							  break;
						  }
					  }
					  
					  Emprunt emprunt = new Emprunt(u, m, raison, dateDepart, dateFin);
					  liste.add(emprunt);
				  }
			  }
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
	}
	
  /**
	 * Cette méthode permet de récupérer les materiels et de les stocker dans une liste
	 * 
	 * @param liste La liste des materiels
	 */
	public static void materiel(List<Materiel> liste) {
		liste.clear();
		try {
	
			File fXmlFile = new File(MainApp.class.getClassLoader().getResource("sauvegardes/materiels.xml").getPath());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
						
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getDocumentElement().getChildNodes();
	
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				  
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element e = (Element) nNode;
					
					int id = Integer.parseInt(e.getElementsByTagName("id").item(0).getTextContent());
					String ref = (e.getElementsByTagName("ref").item(0).getTextContent() == "") ? null : e.getElementsByTagName("ref").item(0).getTextContent();
					String nom = e.getElementsByTagName("nom").item(0).getTextContent();
					String marque = e.getElementsByTagName("marque").item(0).getTextContent();
					Double prix = Double.parseDouble(e.getElementsByTagName("prix").item(0).getTextContent());
					Date dateAchat = (e.getElementsByTagName("dateAchat").item(0).getTextContent() == "") ? null : new SimpleDateFormat("yyyy-MM-dd").parse(e.getElementsByTagName("dateAchat").item(0).getTextContent());
					
					//int instiId = Integer.parseInt(e.getElementsByTagName("instiId").item(0).getTextContent());
					//int stockId = Integer.parseInt(e.getElementsByTagName("stockId").item(0).getTextContent());
					
					if (nNode.getNodeName() == "Telephone" || nNode.getNodeName() == "Tablette") {
						OS os = (e.getElementsByTagName("os").item(0).getTextContent() == "") ? null : OS.valueOf(e.getElementsByTagName("os").item(0).getTextContent());
						Double taille = Double.parseDouble(e.getElementsByTagName("taille").item(0).getTextContent());
						Res resolution = (e.getElementsByTagName("resolution").item(0).getTextContent() == "") ? null : Res.valueOf(e.getElementsByTagName("resolution").item(0).getTextContent());
						
						
						switch (nNode.getNodeName()) {
							case "Telephone":
								liste.add(new Telephone(ref,nom,marque,prix,dateAchat,os,taille,resolution));
								break;
							case "Tablette":
								liste.add(new Tablette(ref,nom,marque,prix,dateAchat,os,taille,resolution));
								break;
						}
					}
					else if (nNode.getNodeName() == "Capteur" || nNode.getNodeName() == "Souris" || nNode.getNodeName() == "ManetteJeu" || nNode.getNodeName() == "ManetteCasque" || nNode.getNodeName() == "Webcam") {
						int sensibilite = (e.getElementsByTagName("sensibilite").item(0).getTextContent() == "") ? 0 : Integer.parseInt(e.getElementsByTagName("sensibilite").item(0).getTextContent());
						connect connectique = (e.getElementsByTagName("connectique").item(0).getTextContent() == "") ? null : connect.valueOf(e.getElementsByTagName("connectique").item(0).getTextContent());
						
						switch (nNode.getNodeName()) {
							case "Capteur":
								liste.add(new Capteur(ref,nom,marque,prix,dateAchat,sensibilite,connectique));
								break;
							case "Souris":
								liste.add(new Souris(ref,nom,marque,prix,dateAchat,sensibilite,connectique));
								break;
							case "ManetteJeu":
								liste.add(new ManetteJeu(ref,nom,marque,prix,dateAchat,sensibilite,connectique));
								break;
							case "ManetteCasque":
								liste.add(new ManetteCasque(ref,nom,marque,prix,dateAchat,sensibilite,connectique, null));
								break;
							case "Webcam":
								liste.add(new Webcam(ref,nom,marque,prix,dateAchat,sensibilite,connectique));
								break;
						}
					}
					else if (nNode.getNodeName() == "CasqueVR" || nNode.getNodeName() == "CasqueAudio") {
						QUALITE qualite = (e.getElementsByTagName("qualite").item(0).getTextContent() == "") ? null : QUALITE.valueOf(e.getElementsByTagName("qualite").item(0).getTextContent());
						
						switch (nNode.getNodeName()) {
							case "CasqueVR":
								liste.add(new CasqueVR(ref,nom,marque,prix,dateAchat,qualite));
								break;
							case "CasqueAudio":
								liste.add(new CasqueAudio(ref,nom,marque,prix,dateAchat,qualite));
								break;
						}
					}
					
					Materiel last = liste.get(liste.size()-1);
					
					if (id != -1) last.setId(id);
				}
			}
		} catch (Exception e) {
			  e.printStackTrace();
		}
	}
	
	/**
	 * Cette méthode permet de récupérer les institutions, les lieux et les stocks
	 * 
	 * @param stocks La liste des stocks
	 * @param lieux La liste des lieux
	 * @param institutions La liste des institutions
	 * @param materiels La liste des materiels
	 * @param utilisateurs La liste des utilisateurs
	 */
	public static void emplacement(List<Stock> stocks, List<Lieu> lieux, List<Institution> institutions, List<Materiel> materiels, List<Utilisateur> utilisateurs) {
		stocks.clear();
		lieux.clear();
		institutions.clear();
		
		try {
	
			File fXmlFile = new File(MainApp.class.getClassLoader().getResource("sauvegardes/emplacements.xml").getPath());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("institution");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				  
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element e = (Element) nNode;
					
					int id = Integer.parseInt(e.getElementsByTagName("id").item(0).getTextContent());
					String name = e.getElementsByTagName("name").item(0).getTextContent();
					String adresse = e.getElementsByTagName("adresse").item(0).getTextContent();
					
					Institution i = new Institution(name, adresse);
					i.setId(id);
					institutions.add(i);
				}
			}
			
			nList = doc.getElementsByTagName("lieu");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				  
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element e = (Element) nNode;
					
					int id = Integer.parseInt(e.getElementsByTagName("id").item(0).getTextContent());
					String nom_lieu = e.getElementsByTagName("nom_lieu").item(0).getTextContent();
					int instiId = Integer.parseInt(e.getElementsByTagName("instiId").item(0).getTextContent());
					int respoId = Integer.parseInt(e.getElementsByTagName("respoId").item(0).getTextContent());
					
					Lieu l = new Lieu(nom_lieu, null, null);
					l.setId(id);
					
					for (Institution institu : institutions) {
						if (instiId == institu.getId()) {
							l.setInsti(institu);
							institu.ajouterLieu(l);
							break;
						}
					}
					for (Utilisateur user : utilisateurs) {
						if (respoId == user.getId()) {
							l.setRespo(user);
							break;
						}
					}
			
					lieux.add(l);
				}
			}
			
			nList = doc.getElementsByTagName("stock");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				  
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element e = (Element) nNode;
					
					int id = Integer.parseInt(e.getElementsByTagName("id").item(0).getTextContent());
					//int id_stock = Integer.parseInt(e.getElementsByTagName("id_stock").item(0).getTextContent());
					int lieuId = Integer.parseInt(e.getElementsByTagName("lieuId").item(0).getTextContent());
					
					Lieu lieu = null;
					for (Lieu l : lieux) {
						if (lieuId == l.getId()) {
							lieu = l;
							break;
						}
					}
					
					Stock s = new Stock(lieu);
					s.setId(id);
					
					for (Lieu l : lieux) {
						if (lieuId == l.getId()) {
							l.ajouterStock(s);
						}
					}
					
					/* Ajouter les materiels au lieu */
					try {
						
						File xmlFile = new File(MainApp.class.getClassLoader().getResource("sauvegardes/materiels.xml").getPath());
						DocumentBuilderFactory dbFactory2 = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilder2 = dbFactory2.newDocumentBuilder();
						Document doc2 = dBuilder2.parse(xmlFile);
						doc2.getDocumentElement().normalize();
						
						NodeList list = doc2.getDocumentElement().getChildNodes();
				
						for (int tmp = 0; tmp < list.getLength(); tmp++) {
							Node node = list.item(tmp);
							  
							if (node.getNodeType() == Node.ELEMENT_NODE) {
								
								Element el = (Element) node;
								
								int matId = Integer.parseInt(el.getElementsByTagName("id").item(0).getTextContent());
								int stockId = Integer.parseInt(el.getElementsByTagName("stockId").item(0).getTextContent());
								
								if (stockId == id) {
									for (Materiel m : materiels) {
										if (matId == m.getId()) {
											s.ajouterMateriel(m);
											break;
										}
									}
								}
								
							}
						}
					} catch (Exception ex) {
						  ex.printStackTrace();
					}
					
					stocks.add(s);
				}
			}
		} catch (Exception e) {
			  e.printStackTrace();
		}
		
		try {
			
			File xmlFile = new File(MainApp.class.getClassLoader().getResource("sauvegardes/materiels.xml").getPath());
			DocumentBuilderFactory dbFactory2 = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder2 = dbFactory2.newDocumentBuilder();
			Document doc2 = dBuilder2.parse(xmlFile);
			doc2.getDocumentElement().normalize();
			
			NodeList list = doc2.getDocumentElement().getChildNodes();
	
			for (int tmp = 0; tmp < list.getLength(); tmp++) {
				Node node = list.item(tmp);
				  
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					
					Element el = (Element) node;
					
					int matId = Integer.parseInt(el.getElementsByTagName("id").item(0).getTextContent());
					int instiId = Integer.parseInt(el.getElementsByTagName("instiId").item(0).getTextContent());
					
					// Institution propriétaire
					for (Materiel m : materiels) {
						if (matId == m.getId()) {
							for (Institution i : institutions) {
								if (instiId == i.getId()) {
									m.achetePar(i);
									break;
								}
							}
							break;
						}
					}
					
				}
			}
		} catch (Exception ex) {
			  ex.printStackTrace();
		}
	}
	
}