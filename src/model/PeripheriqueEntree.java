package model;

import java.util.Date;

/**
 * 
 */

/**
 * Cette classe est utilisée pour représenter un périphérique d'entrée
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class PeripheriqueEntree extends Materiel {

	/**
	 * La sensibilité du périphérique d'entrée
	*/
	private int sensibilite;
	/**
	 * La connectique du périphérique d'entrée
	*/
	public enum connect{Peritel,VGA,USB_C,HDMI};
	private connect connectique;
	
	/**
	 * 
	 * Le constructeur de la classe PeripheriqueEntree
	 * 
	 * @param ref La référence du nouveau périphérique d'entrée
	 * @param nom Le nom du nouveau périphérique d'entrée
	 * @param marque La marque du nouveau périphérique d'entrée
	 * @param prix Le prix du nouveau périphérique d'entrée
	 * @param dateAchat La date d'achat du périphérique d'entrée
	 * @param sens La sensibilite du nouveau périphérique d'entrée
	 * @param ct La connectique du nouveau périphérique d'entrée
	 */
	public PeripheriqueEntree(String ref, String nom, String marque, double prix, Date dateAchat,int sens, connect ct) {
		super(ref, nom, marque, prix, dateAchat);
		this.sensibilite=sens;
		this.connectique=ct;
	}

	/**
	 * Méthode getter retournant la sensibilité du périphérique d'entrée
	 * @return la sensibilite 
	 */
	public int getSensibilite() {
		return sensibilite;
	}

	/**
	 * Méthode setter modifiant la sensibilité du périphérique d'entrée
	 *
	 * @param sensibilite La sensibilite a set
	 */
	public void setSensibilite(int sensibilite) {
		this.sensibilite = sensibilite;
	}

	/**
	 * Méthode getter retournant la connectique du périphérique d'entrée
	 * @return La connectique du peripherique
	 */
	public connect getConnectique() {
		return connectique;
	}

	/**
	 * Méthode setter modifiant la connectique du périphérique d'entrée
	 * 
	 * @param connectique Le nouveau connectique
	 */
	public void setConnectique(connect connectique) {
		this.connectique = connectique;
	}

	@Override
	public void afficher() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifier() {
		// TODO Auto-generated method stub

	}

}
