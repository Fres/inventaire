/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter un capteur
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Capteur extends PeripheriqueEntree {

	/**
	 * 
	 * Le constructeur de la classe Capteur
	 * 
	 * @param ref La référence du nouveau capteur
	 * @param nom Le nom du nouveau capteur
	 * @param marque La marque du nouveau capteur
	 * @param prix Le prix du nouveau capteur
	 * @param dateAchat La date d'achat du nouveau capteur
	 * @param sens La sensibilite du nouveau capteur
	 * @param ct La connectique du nouveau capteur
	 */
	public Capteur(String ref, String nom, String marque, double prix, Date dateAchat, int sens, connect ct) {
		super(ref, nom, marque, prix, dateAchat, sens, ct);
	}

}
