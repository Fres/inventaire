/**
 * 
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Cette classe est utilisée pour représenter un casque VR
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class CasqueVR extends PeripheriqueSortie {

	/**
	 * La liste des manettes du casque VR
	*/
	private ArrayList<ManetteCasque> list_manettes;
	
	/**
	 * 
	 * Le constructeur de la classe CasqueVR
	 * 
	 * @param ref La référence du nouveau casque VR
	 * @param nom Le nom du nouveau casque VR
	 * @param marque La marque du nouveau casque VR
	 * @param prix Le prix du nouveau casque VR
	 * @param dateAchat La date d'achat du nouveau casque VR
	 * @param qualite La qualité du nouveau casque VR
	 */
	public CasqueVR(String ref, String nom, String marque, double prix, Date dateAchat,QUALITE qualite) {
		super(ref, nom, marque, prix, dateAchat, qualite);
		this.list_manettes = new ArrayList<ManetteCasque>(1);
	}

	/**
	 * Cette méthode récupère la liste des manettes du casque VR
	 * @return La liste des manettes du casque VR
	 */
	public ArrayList<ManetteCasque> getList_manettes() {
		return list_manettes;
	}
	
	/**
	 * Cette méthode ajoute une manette à la liste des manettes du casque VR
	 * @param mc La nouvelle manettes de casque VR
	 */
	public void ajouterManettes(ManetteCasque mc) {
		this.list_manettes.add(mc);
	}

	/**
	 * @param list_manettes La liste des manettes du casque VR
	 */
	public void setList_manettes(ArrayList<ManetteCasque> list_manettes) {
		this.list_manettes = list_manettes;
	}
	

}
