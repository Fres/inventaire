package model;
/**
 * 
 */

/**
 * Cette classe abstraite est utilisée pour représenter un enseignant.
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Enseignant extends Utilisateur {

	/**
	 * La matière de l'enseignant
	*/	
	private String enseignement;
	/**
	 * Si l'enseignant est un administratif
	*/	
	private boolean administratif;

	/**
	 * Default constructor
	 */
	public Enseignant() {
		this(null, null, null, null, null, null, false);
	}
	
	/**
	 * Le constructeur de la classe Enseignant
	 * 
	 * @param name Le nom du nouveau Enseignant
	 * @param prenom Le prenom du nouveau Enseignant
	 * @param adresse L'adresse du nouveau Enseignant
	 * @param num_tel Le numéro de téléphone du nouveau Enseignant
	 * @param mail L'adresse email du nouveau Enseignant
	 * @param enseignement La matiere que l'enseignant fait
	 * @param administratif Indique si l'enseignant fait partie de l'administration
	 */
	public Enseignant(String name, String prenom, String adresse, String num_tel, String mail, String enseignement, boolean administratif) {
		super(name, prenom, adresse, num_tel, mail);
		this.setEnseignement(enseignement);
		this.setAdministratif(administratif);
	}

	/**
	 * @return the enseignement
	 */
	public String getEnseignement() {
		return enseignement;
	}

	/**
	 * @param enseignement the enseignement to set
	 */
	public void setEnseignement(String enseignement) {
		this.enseignement = enseignement;
	}

	/**
	 * @return the administratif
	 */
	public boolean isAdministratif() {
		return administratif;
	}

	/**
	 * @param administratif the administratif to set
	 */
	public void setAdministratif(boolean administratif) {
		this.administratif = administratif;
	}
	
	@Override
	public void afficher() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifier() {
		// TODO Auto-generated method stub

	}

}
