/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter une webcam
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Webcam extends PeripheriqueEntree {

	/**
	 * 
	 * Le constructeur de la classe Webcam
	 * 
	 * @param ref La référence de la nouvelle webcam
	 * @param nom Le nom de la nouvelle webcam
	 * @param marque La marque de la nouvelle webcam
	 * @param prix Le prix de la nouvelle webcam
	 * @param dateAchat La date d'achat de la nouvelle webcam
	 * @param sens La sensibilite de la nouvelle webcam
	 * @param ct La connectique de la nouvelle webcam
	 */
	public Webcam(String ref, String nom, String marque, double prix, Date dateAchat,int sens, connect ct) {
		super(ref, nom, marque, prix, dateAchat, sens, ct);
	}

}
