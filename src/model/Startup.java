package model;

/**
 * 
 */

/**
 * Cette classe abstraite est utilisée pour représenter une startup.
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Startup extends Utilisateur {

	/**
	 * La raison sociale de la startup
	*/	
	private String raisonSociale;
	
	/**
	 * Default constructor
	 */
	public Startup() {
		this(null, null, null, null, null, null);
	}
	
	/**
	 * Le constructeur de la classe Startup
	 * 
	 * @param name Le nom du nouveau Startup
	 * @param prenom Le prenom du nouveau Startup
	 * @param adresse L'adresse du nouveau Startup
	 * @param num_tel Le numéro de téléphone du nouveau Startup
	 * @param mail L'adresse email du nouveau Startup
	 * @param raison La raison sociale de la Startup
	 */
	public Startup(String name, String prenom, String adresse, String num_tel, String mail, String raison) {
		super(name, prenom, adresse, num_tel, mail);
		this.setRaisonSociale(raison);
	}

	/**
	 * @return the raisonSociale
	 */
	public String getRaisonSociale() {
		return raisonSociale;
	}

	/**
	 * @param raisonSociale the raisonSociale to set
	 */
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	@Override
	public void afficher() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifier() {
		// TODO Auto-generated method stub

	}

}
