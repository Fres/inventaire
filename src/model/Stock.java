package model;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import javafx.collections.ObservableList;

/**
 * 
 */

/**
 * Cette classe est utilisée pour représenter un stock de matériel
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Stock {
	
	/**
	 * Generateur d'id auto-incremente
	 */
	private static final AtomicInteger ID_FACTORY = new AtomicInteger();
	
	/**
	 * L'id du stock
	 */
	private int id;
	
	/**
	 * L'identifiant du stock
	*/
	private int id_stock;
	
	/**
	 * La liste de matériel du stock
	*/
	private ArrayList<Materiel> liste_m;
	
	/**
	 * Le lieu du stock
	*/
	private Lieu lieu;

	/**
	 * Compteur d'instance stock
	*/
	private static int compteur_s=0;
	
	/**
	 * Le constructeur de la classe Stock
	 * 
	 * @param lieu Le lieu du nouveau stock
	 */
	public Stock(Lieu lieu) {
		this.id = ID_FACTORY.getAndIncrement();
		this.liste_m = new ArrayList<Materiel>();
		this.lieu = lieu;
		this.id_stock=compteur_s;
		compteur_s++;
	}

	/**
	 * @return L'id du stock
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id Le nouvel id du stock
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return L'identifiant du stock
	 */
	public int getId_stock() {
		return id_stock;
	}

	/**
	 * @param id_stock Le nouveau identifiant du stock
	 */
	public void setId_stock(int id_stock) {
		this.id_stock = id_stock;
	}
	
	/**
	 * @return Le lieu du stock
	 */
	public Lieu getLieu() {
		return lieu;
	}

	/**
	 * @param lieu Le nouveau lieu du stock
	 */
	public void setLieu(Lieu lieu) {
		this.lieu = lieu;
	}
	
	
	/**
	 * @return the liste_m
	 */
	public ArrayList<Materiel> getListe_m() {
		return liste_m;
	}

	/**
	 * @param liste_m the liste_m to set
	 */
	public void setListe_m(ArrayList<Materiel> liste_m) {
		this.liste_m = liste_m;
	}

	/**
	 * Cette méthode permet de vérifier l'égalité entre deux stocks
	 * 
	 * @param s2 Le stock 2 à comparé
	 * @return True si egalite, False sinon
	 */	
	public boolean equals(Stock s2) {
		return (this.id_stock == s2.getId_stock());
	}
	
	/**
	 * Cette méthode permet d'ajouter un matériel au stock
	 * 
	 * @param m Le matériel à ajouter
	 */
	public void ajouterMateriel(Materiel m) {
		this.liste_m.add(m);
		m.stockeDans(this);
	}
	
	/**
	 * Cette méthode permet d'ajouter un lot de matériel au stock
	 * 
	 * @param lot Le lot de matériels à ajouter
	 */
	public void ajouterMateriel(ObservableList<Materiel> lot) {
		this.liste_m.addAll(lot);
		for(Materiel m: lot) 
			m.stockeDans(this);
	}
	
	/**
	 * Cette méthode permet de modifier un matériel du stock
	 * 
	 * @param m Le matériel à modifier
	 */
	public void modifierMateriel(Materiel m) {
		
	}
	
	/**
	 * Cette méthode permet de supprimer un materiel du stock
	 * 
	 * @param m Le materiel à supprimer
	 */
	public void supprimerMateriel(Materiel m){
		int i=0;
		boolean supprimer=false;
		while(i<this.liste_m.size() && !supprimer) {
			if(this.liste_m.get(i).equals(m)) {
				this.liste_m.remove(i);
				m.plusStockeDans(this);
				supprimer=true;
			}
		}
	}
	
	/**
	 * Cette méthode permet d'affecter un lieu à un matériel
	 * 
	 * @param lieu Le lieu à affecter
	 */
	public void stockerA(Lieu lieu) {
		this.setLieu(lieu);
	}
	
	
}
