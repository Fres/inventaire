package model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Helper class to wrap a list of emprunts. This is used for saving the
 * list of emprunts to XML.
 * 
 */
@XmlRootElement(name = "startups")
public class StartupListWrapper {

    private List<Startup> startups;

    @XmlElement(name = "startup")
    public List<Startup> getStartups() {
        return startups;
    }

    public void setStartups(List<Startup> startups) {
        this.startups = startups;
    }
}