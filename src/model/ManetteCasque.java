/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter une manette d'un casque
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ManetteCasque extends PeripheriqueEntree {

	/**
	 * Le casque VR auquel appartient la manette
	 */
	private CasqueVR casqueVr;
	/**
	 * 
	 * Le constructeur de la classe ManetteCasque
	 * 
	 * @param ref La référence de la nouvelle manette d'un casque vr
	 * @param nom Le nom de la nouvelle manette d'un casque vr
	 * @param marque La marque de la nouvelle manette d'un casque vr
	 * @param prix Le prix de la nouvelle manette d'un casque vr
	 * @param dateAchat La date d'achat de la nouvelle manette d'un casque vr
	 * @param sens La sensibilite de la nouvelle manette d'un casque vr
	 * @param ct La connectique de la nouvelle manette d'un casque vr
	 * @param cvr Le casque VR auquel va appartenir la nouvelle manette d'un casque vr
	 */
	public ManetteCasque(String ref, String nom, String marque, double prix, Date dateAchat,int sens, connect ct, CasqueVR cvr) {
		super(ref, nom, marque, prix, dateAchat, sens, ct);
		this.casqueVr=cvr;
	}
	
	/**
	 * Méthode getter retournant le casque VR de la manette
	 * @return Le casque VR de la manette
	 */
	public CasqueVR getCasqueVr() {
		return casqueVr;
	}
	
	/**
	 * Méthode setter permettant d'affecter de changer le casque VR de la manette
	 * 
	 * @param casqueVr Le nouveau casque VR de la manette
	 */
	public void setCasqueVr(CasqueVR casqueVr) {
		this.casqueVr = casqueVr;
	}
	

}
