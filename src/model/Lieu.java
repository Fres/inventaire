package model;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 */

/**
 * Cette classe est utilisée pour représenter un lieu
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Lieu {

	/**
	 * Generateur d'id auto-incremente
	 */
	private static final AtomicInteger ID_FACTORY = new AtomicInteger();
	
	/**
	 * L'id du lieu
	 */
	private int id;
	
	/**
	 * Le nom du lieu de stockage
	*/	
	private String nom_lieu;
	
	/**
	 * La liste des stocks présentent
	*/
	private ArrayList<Stock> liste_s;
	
	/**
	 * L'institution où est le lieu
	 */
	private Institution insti;
	
	/**
	 * Le responsable du lieu de stockage
	*/	
	public Utilisateur respo;

	/**
	 * Le constructeur de la classe stock
	 * 
	 * @param nom_lieu Le nom du nouveau lieu de stockage
	 * @param insti L'institution a laquelle appartient le lieu
	 * @param user Le responsable du nouveau lieu de stockage
	 */
	public Lieu(String nom_lieu,Institution insti,Utilisateur user) {
		this.id = ID_FACTORY.getAndIncrement();
		this.nom_lieu = nom_lieu;
		this.insti = insti;
		this.liste_s = new ArrayList<Stock>();
		this.respo=user;
	}

	/**
	 * @return L'id du lieu
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id Le nouvel id du lieu
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return Le nom du lieu de stockage
	 */
	public String getNom_lieu() {
		return nom_lieu;
	}

	/**
	 * @param nom_lieu Le nouveau nom du lieu de stockage
	 */
	public void setNom_lieu(String nom_lieu) {
		this.nom_lieu = nom_lieu;
	}
	
	
	
	/**
	 * @return the insti
	 */
	public Institution getInsti() {
		return insti;
	}

	/**
	 * @param insti the insti to set
	 */
	public void setInsti(Institution insti) {
		this.insti = insti;
	}
	

	/**
	 * @return the respo
	 */
	public Utilisateur getRespo() {
		return respo;
	}

	/**
	 * @param respo the respo to set
	 */
	public void setRespo(Utilisateur respo) {
		this.respo = respo;
	}
	
	

	/**
	 * @return the liste_s
	 */
	public ArrayList<Stock> getListe_s() {
		return liste_s;
	}

	/**
	 * Cette méthode permet de retourner les caractéristiques d'un lieu de stockage
	 * 
	 */	
	public String toString() {
		return "Lieu "+this.nom_lieu;
	}
	
	/**
	 * Cette méthode permet de vérifier l'égalité entre deux lieux de stockages
	 * 
	 * @param l2 Le lieu 2 à comparer
	 * @return True si egalite, False sinon
	 */	
	public boolean equals(Lieu l2) {
		return (this.nom_lieu == l2.getNom_lieu());
	}
	
	/**
	 * Cette méthode permet d'ajouter un stock au lieu lieu
	 * 
	 * @param s Le stock à ajouté
	 */
	public void ajouterStock(Stock s) {
		this.liste_s.add(s);
		s.stockerA(this);
	}
	
	/**
	 * Cette méthode permet de modifier un stock au lieu
	 * 
	 * @param s Le stock à modifier
	 */
	public void modifierStock(Stock s) {
		
	}
	
	/**
	 * Cette méthode permet de supprimer un stock au lieu
	 * 
	 * @param s Le stock à supprimer
	 * @throws Exception Si le stock n'existe pas
	 */
	public void supprimerStock(Stock s) throws Exception {
		int i=0;
		boolean supprimer=false;
		while(i<this.liste_s.size() && !supprimer) {
			if(this.liste_s.get(i).equals(s)) {
				this.liste_s.remove(i);
				supprimer=true;
			}
		}
		if(!supprimer) throw new Exception("Stock non trouvé");
	}
	
	/**
	 * Cette méthode permet de changer un responsable
	 * 
	 * @param user Le nouveau responsable du lieu
	 */
	public void changerRespo(Utilisateur user) {
		this.respo=user;
	}
	
	
}
