package model;

/**
 * 
 */

/**
 * Cette classe abstraite est utilisée pour représenter un etudiant.
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Etudiant extends Utilisateur {

	/**
	 * L'année de formation de l'étudiant
	*/	
	private String annee;
	/**
	 * Le nom de la formation de l'étudiant
	*/	
	private String formation;

	/**
	 * Default constructor
	 */
	public Etudiant() {
		this(null, null, null, null, null, null, null);
	}
	
	/**
	 * Le constructeur de la classe Etudiant
	 * 
	 * @param name Le nom du nouveau Etudiant
	 * @param prenom Le prenom du nouveau Etudiant
	 * @param adresse L'adresse du nouveau Etudiant
	 * @param num_tel Le numéro de téléphone du nouveau Etudiant
	 * @param mail L'adresse email du nouveau Etudiant
	 * @param annee L'année de formation du nouvel Etudiant
	 * @param formation Le nom de formation du nouvel Etudiant
	 */
	public Etudiant(String name, String prenom, String adresse, String num_tel, String mail, String annee, String formation) {
		super(name, prenom, adresse, num_tel, mail);
		this.setAnnee(annee);
		this.setFormation(formation);
	}

	/**
	 * @return the annee
	 */
	public String getAnnee() {
		return annee;
	}

	/**
	 * @param annee the annee to set
	 */
	public void setAnnee(String annee) {
		this.annee = annee;
	}

	/**
	 * @return the formation
	 */
	public String getFormation() {
		return formation;
	}

	/**
	 * @param formation the formation to set
	 */
	public void setFormation(String formation) {
		this.formation = formation;
	}

	@Override
	public void afficher() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifier() {
		// TODO Auto-generated method stub

	}

}
