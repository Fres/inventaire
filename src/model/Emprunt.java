package model;
import java.util.Date;

/**
 * 
 */

/**
 * Cette classe est utilisée pour représenter un emprunt
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Emprunt {
	
	/**
	 * L'emprunteur
	*/	
	private Utilisateur utilisateur;
	
	/**
	 * Le matériel emprunté
	*/	
	private Materiel materiel;
	
	/**
	 * La raison de l'emprunt
	*/	
	private String raison;
	
	/**
	 * La date de l'emprunt
	*/	
	private Date dateEmprunt;

	/**
	 * La date limite de retour
	*/	
	private Date dateRetour;

	/**
	 * 
	 * Le constructeur de la classe Emprunt
	 * 
	 * @param utilisateur L'emprunteur
	 * @param materiel Le matériel emprunté
	 * @param raison La raison de l'emprunt
	 * @param dateEmprunt La date à laquelle le matériel a été emprunté
	 * @param dateRetour La date limite de retour du matériel
	 */
	public Emprunt(Utilisateur utilisateur, Materiel materiel, String raison,Date dateEmprunt,Date dateRetour) {
		this.utilisateur = utilisateur;
		this.materiel = materiel;
		if(materiel!=null)
			this.materiel.estEmprunter(this);
		this.raison = raison;
		this.dateEmprunt = dateEmprunt;
		this.dateRetour = dateRetour;
	}

	public Emprunt() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return L'emprunteur
	 */
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	/**
	 * @param user Le nouveau emprunteur du matériel
	 */
	public void setUtilisateur(Utilisateur user) {
		this.utilisateur = user;
	}

	/**
	 * @return Le matériel emprunter
	 */
	public Materiel getMat() {
		return materiel;
	}

	/**
	 * @param mat Le nouveau matériel emprunter
	 */
	public void setMat(Materiel mat) {
		this.materiel = mat;
	}

	/**
	 * @return La raison de l'emprunt du matériel
	 */
	public String getRaison() {
		return raison;
	}

	/**
	 * @param raison La nouvelle raison de l'emprunt
	 */
	public void setRaison(String raison) {
		this.raison = raison;
	}

	/**
	 * @return La date de l'emprunt du matériel
	 */
	public Date getDateEmprunt() {
		return dateEmprunt;
	}

	/**
	 * @param dateEmprunt La nouvelle date d'emprunt du matériel
	 */
	public void setDateEmprunt(Date dateEmprunt) {
		this.dateEmprunt = dateEmprunt;
	}

	/**
	 * @return La date retour limite du matériel
	 */
	public Date getDateRetour() {
		return dateRetour;
	}

	/**
	 * @param dateRetour La nouvelle date de retour limite du matériel
	 */
	public void setDateRetour(Date dateRetour) {
		this.dateRetour = dateRetour;
	}
	
	/**
	 * Cette méthode permet de retourner les caractéristiques d'un emprunt
	 * 
	 * @return Retourne l'objet sous forme de chaine de caracteres
	 */
	public String toString() {
		return this.utilisateur.getNom()+" a emprunté le matériel : "+this.materiel.getNom();
	}
	
	/**
	 * Cette méthode permet de vérifier si l'utilisateur a dépassé la date de retour du matériel
	 * 
	 * @return True si en retard, False sinon
	 */
	public boolean estRetard() {
		Date today = new Date();
		boolean retard = false;
		
		if(today.compareTo(this.dateRetour) > 0)
			retard=true;
		
		return retard;
	}
	
	
	
	/**
	 * Cette méthode permet de vérifier l'égalité entre deux emprunts. On se base sur l'égalité des matériels des deux emprunts
	 * 
	 * @param e2 L'emprunt 2 à comparer
	 * @return True si le material est egal, False sinon
	 */	
	public boolean equals(Emprunt e2) {
		return (this.materiel.equals(e2.getMat()));
	}
	
	
	
}
