package model;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 */

/**
 * Cette classe abstraite est utilisée pour représenter un matériel
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public abstract class Materiel {
	
	/**
	 * Generateur d'id auto-incremente
	 */
	private static final AtomicInteger ID_FACTORY = new AtomicInteger();
	
	/**
	 * L'id du materiel
	 */
	private int id;
	/**
	 * La référence du matériel
	*/
	private String ref;
	/**
	 * Le nom du matériel
	*/
	private String nom;
	/**
	 * La marque du matériel
	*/
	private String marque;
	/**
	 * Le prix d'achat du matériel
	*/
	private double prix;
	/**
	 * La liste des états du matériel
	*/
    public enum Etat {Neuf,Deteriore,HS};	
    private Etat etat;
	/**
	 * La date d'achat du matériel
	*/
	private Date dateAchat;
	
	/**
	 * L'institution ayant acheté le matériel
	*/
	private Institution insti;
	
	/**
	 * Le stock dans laquel est affecté le matériel
	*/
	private Stock stock;
	
	/**
	 * L'emprunt où est affecté le matériel
	*/
	private Emprunt emprunt;
	
	/**
	 * La disponibilité du matériel
	*/
	private boolean disponible;
	/**
	 * 
	 * Le constructeur de la classe matériel
	 * 
	 * @param ref La référence du nouveau matériel
	 * @param nom Le nom du nouveau matériel
	 * @param marque La marque du nouveau matériel
	 * @param prix Le prix du nouveau matériel
	 * @param dateAchat La date d'achat du nouveau matériel
	 */
	public Materiel(String ref, String nom, String marque, double prix, Date dateAchat) {
		this.id = ID_FACTORY.getAndIncrement();
		this.ref = ref;
		this.nom = nom;
		this.marque = marque;
		this.prix = prix;
		this.dateAchat = dateAchat;
		this.disponible=true;
		this.etat=Etat.Neuf;
	}
	
	/**
	 * @return L'id du materiel
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id Le nouvel id du materiel
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return La référence du matériel
	 */
	public String getRef() {
		return ref;
	}
	/**
	 * @param ref La nouvelle référence du matériel
	 */
	public void setRef(String ref) {
		this.ref = ref;
	}
	/**
	 * @return Le nom du matériel
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom Le nouveau nom du matériel
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return La marque du matériel
	 */
	public String getMarque() {
		return marque;
	}
	/**
	 * @param marque La nouvelle marque du matériel
	 */
	public void setMarque(String marque) {
		this.marque = marque;
	}
	/**
	 * @return Le prix du matériel
	 */
	public double getPrix() {
		return prix;
	}
	/**
	 * @param prix Le nouveau prix du matériel
	 */
	public void setPrix(double prix) {
		this.prix = prix;
	}
	/**
	 * @return La date d'achat du matériel
	 */
	public Date getDateAchat() {
		return dateAchat;
	}
	/**
	 * @param dateAchat La nouvelle data d'achat du matériel
	 */
	public void setDateAchat(Date dateAchat) {
		this.dateAchat = dateAchat;
	}
	
	
	/**
	 * @return the etat
	 */
	public Etat getEtat() {
		return etat;
	}
	/**
	 * @param etat the etat to set
	 */
	public void setEtat(Etat etat) {
		this.etat = etat;
	}
	
	/**
	 * @return the insti
	 */
	public Institution getInsti() {
		return insti;
	}
	/**
	 * @param insti the insti to set
	 */
	public void setInsti(Institution insti) {
		this.insti = insti;
	}
	/**
	 * @return the stock
	 */
	public Stock getStock() {
		return stock;
	}
	/**
	 * @param stock the stock to set
	 */
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	/**
	 * @return the emprunt
	 */
	public Emprunt getEmprunt() {
		return emprunt;
	}
	/**
	 * @param emprunt the emprunt to set
	 */
	public void setEmprunt(Emprunt emprunt) {
		this.emprunt = emprunt;
	}
	/**
	 * Cette méthode permet de vérifier si le matériel est disponible
	 * 
	 * @return True si disponible, False sinon
	 */
	public boolean estDispo() {
		return this.disponible;
	}
	
	/**
	 * Cette méthode permet de rendre disponible ou indisponible un matériel
	 * 
	 * @param b Disponibilité (true) ou indisponibilité (false) du matériel
	 */
	public void dispoMateriel(boolean b) {
		this.disponible=b;
	}
	
	/**
	 * Cette méthode permet d'affecter une institution à un matériel
	 * 
	 * @param insti L'institution ayant acheté le matériel
	 */
	public void achetePar(Institution insti) {
		this.setInsti(insti);
	}
	
	/**
	 * Cette méthode permet d'affecter un stock à un matériel
	 * 
	 * @param s Le stock dans lequel sera affecté le matériel
	 */
	public void stockeDans(Stock s) {
		this.setStock(s);
	}
	
	/**
	 * Cette méthode permet d'enlever un matériel d'un stock
	 * 
	 * @param s Le stock dans lequel on veut retirer le matériel
	 */
	public void plusStockeDans(Stock s) {
		this.setStock(null);
	}
	
	/**
	 * Cette méthode permet d'affecter un emprunt au matériel
	 * 
	 * @param e L'emprunt où se trouve le matériel
	 */
	public void estEmprunter(Emprunt e) {
		this.setEmprunt(e);
		e.setMat(this);
		this.dispoMateriel(false);
	}
	
	/**
	 * Cette méthode permet de retirer le matériel de l'emprunt
	 * 
	 * @param e L'emprunt a retirer de la liste dans Materiel
	 */
	public void retirerEmprunter(Emprunt e) {
		this.setEmprunt(null);
		e.setMat(null);
		this.dispoMateriel(true);
	}
	
	/**
	 * Cette méthode permet de vérifier l'égalité entre deux matériels. On se base sur l'égalité de la référence entre les deux emprunts
	 * 
	 * @param m2 Le matériel 2 à comparer
	 * @return True si m2 est egal, False sinon
	 */	
	public boolean equals(Materiel m2) {
		return (this.ref == m2.getRef());
	}
	
	public String toString() {
		return this.ref+" - "+this.nom;
	}
	
	/**
	* Cette méthode abstraite est utilisée pour afficher les détails d'un matériel
	*
	*/
	public abstract void afficher();
	
	/**
	* Cette méthode abstraite est utilisée pour modifier les détails d'un matériel
	*
	*/
	public abstract void modifier();
}
