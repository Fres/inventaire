/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter une manette de jeu
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ManetteJeu extends PeripheriqueEntree {

	/**
	 * 
	 * Le constructeur de la classe ManetteJeu
	 * 
	 * @param ref La référence de la nouvelle manette de jeu
	 * @param nom Le nom de la nouvelle manette de jeu
	 * @param marque La marque de la nouvelle manette de jeu
	 * @param prix Le prix de la nouvelle manette de jeu
	 * @param dateAchat La date d'achat de la nouvelle manette de jeu
	 * @param sens La sensibilite de la nouvelle manette de jeu
	 * @param ct La connectique de la nouvelle manette de jeu
	 */
	public ManetteJeu(String ref, String nom, String marque, double prix, Date dateAchat,int sens, connect ct) {
		super(ref, nom, marque, prix, dateAchat, sens, ct);
	}

}
