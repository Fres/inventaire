/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter une tablette
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Tablette extends Appareil {

	/**
	 * 
	 * Le constructeur de la classe Tablette
	 * 
	 * @param ref La référence de la nouvelle tablette
	 * @param nom Le nom de la nouvelle tablette
	 * @param marque La marque de la nouvelle tablette
	 * @param prix Le prix de la nouvelle tablette
	 * @param dateAchat La date d'achat de la nouvelle tablette
	 * @param os L'OS de la nouvelle tablette
	 * @param taille La taille de la nouvelle tablette
	 * @param res La resolution de la nouvelle tablette
	 */
	public Tablette(String ref, String nom, String marque, double prix, Date dateAchat,OS os, Double taille, Res res) {
		super(ref, nom, marque, prix, dateAchat, os, taille, res);
	}

}
