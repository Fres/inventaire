package model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Helper class to wrap a list of emprunts. This is used for saving the
 * list of emprunts to XML.
 * 
 */
@XmlRootElement(name = "enseignants")
public class EnseignantListWrapper {

    private List<Enseignant> enseignants;

    @XmlElement(name = "enseignant")
    public List<Enseignant> getEnseignants() {
        return enseignants;
    }

    public void setEnseignants(List<Enseignant> enseignants) {
        this.enseignants = enseignants;
    }
}