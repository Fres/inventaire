package model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Helper class to wrap a list of emprunts. This is used for saving the
 * list of emprunts to XML.
 * 
 */
@XmlRootElement(name = "etudiants")
public class EtudiantListWrapper {

    private List<Etudiant> etudiants;

    @XmlElement(name = "etudiant")
    public List<Etudiant> getEtudiants() {
        return etudiants;
    }

    public void setEtudiants(List<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }
}