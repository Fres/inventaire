package model;

import java.util.Date;

/**
 * 
 */

/**
 * Cette classe est utilisée pour représenter un appareil
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Appareil extends Materiel {

	/**
	 * La liste des OS possible d'un appareil
	*/
    public enum OS {iOS,Android};	
    private OS os;

	/**
	 * La taille du matériel
	*/
	private Double taille;
	/**
	 * La résolution du matériel
	*/
	public enum Res{UltraHD,QuadHD,FullHD,HD,SD};
	private Res resolution;
	
	/**
	 * 
	 * Le constructeur de la classe appareil
	 * 
	 * @param ref La référence du nouveau appareil
	 * @param nom Le nom du nouveau appareil
	 * @param marque La marque du nouveau appareil
	 * @param prix Le prix du nouveau appareil
	 * @param dateAchat La date d'achat du nouveau appareil
	 * @param os L'OS du nouvea materiel
	 * @param taille La taille du nouveau materiel
	 * @param res La resolution du nouveau materiel
	 */
	public Appareil(String ref, String nom, String marque, double prix, Date dateAchat,OS os, Double taille, Res res ) {
		super(ref, nom, marque, prix, dateAchat);
		this.os=os;
		this.taille=taille;
		this.resolution=res;
	}

	/**
	 * @return l'OS de l'appareil
	 */
	public OS getOs() {
		return os;
	}
	/**
	 * @param os L'OS a set
	 */
	public void setOs(OS os) {
		this.os = os;
	}

	/**
	 * @return la taille de l'appareil
	 */
	public Double getTaille() {
		return taille;
	}
	/**
	 * @param taille La taille set
	 */
	public void setTaille(Double taille) {
		this.taille = taille;
	}

	/**
	 * @return la resolution de l'appareil
	 */
	public Res getResolution() {
		return resolution;
	}
	/**
	 * @param res La resolution a set
	 */
	public void setResolution(Res res) {
		this.resolution = res;
	}

	@Override
	public void afficher() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifier() {
		// TODO Auto-generated method stub

	}

}
