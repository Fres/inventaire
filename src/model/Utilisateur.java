package model;


import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 */

/**
 * Cette classe abstraite est utilisée pour représenter un utilisateur.
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public abstract class Utilisateur {
	
	/**
	 * Generateur d'id auto-incremente
	 */
	private static final AtomicInteger ID_FACTORY = new AtomicInteger();
	
	/**
	 * L'id de l'utilisateur
	 */
	private int id;
	
	/**
	 * Le nom de l'utilisateur
	*/	
	private String nom;
	/**
	 * Le prenom de l'utilisateur
	*/	
	private String prenom;
	/**
	 * L'adresse de l'utilisateur
	*/	
	private String adresse;
	/**
	 * le numéro de téléphone de l'utilisateur
	*/	
	private String num_tel;
	/**
	 * L'adresse email de l'utilisateur
	*/	
	private String mail;
	
	/**
	 * La liste des emprunts de l'utilisateur
	*/	
	private ArrayList<Emprunt> liste_emprunts;
	
	/**
	 * Le constructeur de la classe Utilisateur
	 * 
	 * @param nom Le nom du nouveau utilisateur
	 * @param prenom Le prenom du nouveau utilisateur
	 * @param adresse L'adresse du nouveau utilisateur
	 * @param num_tel Le numéro de téléphone du nouveau utilisateur
	 * @param mail L'adresse email du nouveau utilisateur 
	 */
	public Utilisateur(String nom, String prenom, String adresse, String num_tel, String mail) {
		this.id = ID_FACTORY.getAndIncrement();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.num_tel = num_tel;
		this.mail = mail;
	}
	
	/**
	 * @return L'id de l'utilisateur
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id Le nouvel id de l'utilisateur
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return Le nom de l'utilisateur
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom Le nouveau nom de l'utilisateur
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return Le prénom de l'utilisateur
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom Le nouveau prenom de l'utilisateur
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return L'adresse de l'utilisateur
	 */
	public String getAdresse() {
		return adresse;
	}
	/**
	 * @param adresse La nouvelle adresse de l'utilisateur
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	/**
	 * @return Le numéro de téléphone de l'utilisateur 
	 */
	public String getNum_tel() {
		return num_tel;
	}
	/**
	 * @param num_tel Le nouveau numéro de téléphone de l'utilisateur
	 */
	public void setNum_tel(String num_tel) {
		this.num_tel = num_tel;
	}
	/**
	 * @return L'adresse mail de l'utilisateur
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail La nouvelle adresse mail de l'utilisateur
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	/**
	 * Cette méthode permet de vérifier l'égalité entre deux utilisateurs
	 * 
	 * @param u2 L'utilisateur 2 à comparer
	 * @return True si egalite, False sinon
	 */	
	public boolean equals(Utilisateur u2) {
		return (this.getNom() == u2.getNom() && this.getPrenom()==u2.getPrenom());
	}
	
	/**
	 * Cette méthode permet d'ajouter un emprunt à l'utilisateur
	 * 
	 * @param m Le matériel emprunté par l'utilisateur
	 * @param raison La raison de l'emprunt
	 * @param emprunt La date d'emprunt
	 * @param retour La date limite de retour de l'emprunt
	 */
	public void emprunter(Materiel m,String raison, Date emprunt,Date retour) {
		Emprunt e = new Emprunt(this,m,raison,emprunt,retour);
		this.liste_emprunts.add(e);
		m.estEmprunter(e);
	}
	
	/**
	 * Cette méthode permet de modifier un emprunt
	 * 
	 * @param e L'emprunt de l'utilisateur à modifier
	 */
	public void modifierEmprunt(Emprunt e) {
		
	}
	
	/**
	 * Cette méthode permet de rendre un matériel emprunté par l'utilisateur
	 * 
	 * @param e L'emprunt que l'utilisateur veut rendre
	 * @throws Exception Si l'emprunt n'existe pas
	 */
	public void rendreEmprunt(Emprunt e) throws Exception {
		int i=0;
		boolean supprimer=false;
		while(i<this.liste_emprunts.size() && !supprimer) {
			if(this.liste_emprunts.get(i).equals(e)) {
				this.liste_emprunts.remove(i);
				e.getMat().retirerEmprunter(e);
				supprimer=true;
			}
		}
		if(!supprimer) throw new Exception("Emprunt non trouvé");
	}
	
	/**
	* Cette méthode abstraite est utilisée pour afficher les détails d'un utilisateur
	*
	*/
	public abstract void afficher();
	
	/**
	* Cette méthode abstraite est utilisée pour modifier les détails d'un utilisateur
	*
	*/
	public abstract void modifier();
	
	
}
