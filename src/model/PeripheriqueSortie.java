package model;

import java.util.Date;


/**
 * 
 */

/**
 * Cette classe est utilisée pour représenter un périphérique de sortie
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class PeripheriqueSortie extends Materiel {

	/**
	 * La qualité du périphérique de sortie
	*/
	public enum QUALITE{TresBon, Bon, Mauvais};
	private QUALITE qualite;

	
	/**
	 * 
	 * Le constructeur de la classe périphérique de sortie
	 * 
	 * @param ref La référence du nouveau périphérique de sortie
	 * @param nom Le nom du nouveau périphérique de sortie
	 * @param marque La marque du nouveau périphérique de sortie
	 * @param prix Le prix du nouveau périphérique de sortie
	 * @param dateAchat La date d'achat du périphérique de sortie
	 * @param qualite La qualité du nouveau périphérique de sortie
	 */
	public PeripheriqueSortie(String ref, String nom, String marque, double prix, Date dateAchat,QUALITE qualite) {
		super(ref, nom, marque, prix, dateAchat);
		this.qualite=qualite;

	}

	/**
	 * Méthode retournant la qualité du périphérique de sortie
	 * @return La qualité du périphérique de sortie
	 */
	public QUALITE getQualite() {
		return qualite;
	}

	/**
	 * Méthode modifiant la qualité du périphérique de sortie
	 * @param qualite La nouvelle qualité du périphérique de sortie
	 */
	public void setQualite(QUALITE qualite) {
		this.qualite = qualite;
	}


	@Override
	public void afficher() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifier() {
		// TODO Auto-generated method stub

	}

}
