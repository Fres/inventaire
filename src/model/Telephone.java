/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter un téléphone
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Telephone extends Appareil {

	/**
	 * 
	 * Le constructeur de la classe Telephone
	 * 
	 * @param ref La référence du nouveau téléphone
	 * @param nom Le nom du nouveau téléphone
	 * @param marque La marque du nouveau téléphone
	 * @param prix Le prix du nouveau téléphone
	 * @param dateAchat La date d'achat du nouveau téléphone
	 * @param os L'OS du nouvea téléphone
	 * @param taille La taille du nouveau téléphone
	 * @param res La resolution du nouveau téléphone
	 */
	public Telephone(String ref, String nom, String marque, double prix, Date dateAchat, OS os, Double taille, Res res) {
		super(ref, nom, marque, prix, dateAchat, os, taille, res);
	}

}
