/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter une souris
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Souris extends PeripheriqueEntree {

	/**
	 * 
	 * Le constructeur de la classe Souris
	 * 
	 * @param ref La référence de la nouvelle souris
	 * @param nom Le nom de la nouvelle souris
	 * @param marque La marque de la nouvelle souris
	 * @param prix Le prix de la nouvelle souris
	 * @param dateAchat La date d'achat de la nouvelle souris
	 * @param sens La sensibilite de la nouvelle souris
	 * @param ct La connectique de la nouvelle souris
	 */
	public Souris(String ref, String nom, String marque, double prix, Date dateAchat,int sens, connect ct) {
		super(ref, nom, marque, prix, dateAchat, sens, ct);
	}

}
