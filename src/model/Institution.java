package model;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 */

/**
 * Cette classe est utilisée pour représenter une institution
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class Institution {
	
	/**
	 * Generateur d'id auto-incremente
	 */
	private static final AtomicInteger ID_FACTORY = new AtomicInteger();
	
	/**
	 * L'id de l'institution
	 */
	private int id;
	
	/**
	 * Le nom de l'institution
	*/	
	private String name;

	/**
	 * L'adresse de l'institution
	*/	
	private String adresse;
	
	/**
	 * La liste des lieux de stockage de l'institution
	*/
	private ArrayList<Lieu> liste_l;

	/**
	 * Le constructeur de la classe institution
	 * 
	 * @param name Le nom de la nouvelle institution
	 * @param adresse L'adresse de la nouvelle institution
	 */
	public Institution(String name, String adresse) {
		this.id = ID_FACTORY.getAndIncrement();
		this.name = name;
		this.adresse = adresse;
		this.liste_l= new ArrayList<Lieu>();
	}

	/**
	 * @return L'id de l'institution
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id Le nouvel id de l'institution
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return Le nom de l'institution
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name Le nouveau nom de l'institution
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return L'adresse de l'institution
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse La nouvelle adresse de l'institution
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
		
	/**
	 * @return the liste_l
	 */
	public ArrayList<Lieu> getListe_l() {
		return liste_l;
	}

	/**
	 * Cette méthode permet d'ajouter un nouveau lieu de stockage à l'institution
	 * 
	 * @param l Le lieu de stockage a ajouté
	 */
	public void ajouterLieu(Lieu l) {
		this.liste_l.add(l);
	}
	
	/**
	 * Cette méthode permet de modifier un lieu de stockage de l'institution
	 * 
	 * @param l Le lieu de stockage a modifier
	 */
	public void modifierLieu(Lieu l) {
		
	}
	
	/**
	 * Cette méthode permet de supprimer un lieu de stockage de l'institution
	 * 
	 * @param l Le lieu de stockage à supprimer
	 * @throws Exception Si le lieu de stockage n'existe pas
	 */
	public void supprimerLieu(Lieu l) throws Exception {
		int i=0;
		boolean supprimer=false;
		while(i<this.liste_l.size() && !supprimer) {
			if(this.liste_l.get(i).equals(l)) {
				this.liste_l.remove(i);
				supprimer=true;
			}
		}
		if(!supprimer) throw new Exception("Lieu non trouvé");
	}
	
	/**
	 * Cette méthode permet d'afficher les lieux de stockage de l'institution
	 * 
	 */
	public void afficherLieux() {
		for(int i=0;i<this.liste_l.size();i++)
			System.out.println(this.liste_l.get(i).toString());
	}
	
	
}
