/**
 * 
 */
package model;

import java.util.Date;

/**
 * Cette classe est utilisée pour représenter un casque audio
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class CasqueAudio extends PeripheriqueSortie {

	/**
	 * 
	 * Le constructeur de la classe CasqueAudio
	 * 
	 * @param ref La référence du nouveau casque studio
	 * @param nom Le nom du nouveau casque studio
	 * @param marque La marque du nouveau casque studio
	 * @param prix Le prix du nouveau casque studio
	 * @param dateAchat La date d'achat du nouveau casque studio
	 * @param qualite La qualité du nouveau casque studio
	 */
	public CasqueAudio(String ref, String nom, String marque, double prix, Date dateAchat, QUALITE qualite) {
		super(ref, nom, marque, prix, dateAchat, qualite);
	}

}
