package controller;

import model.*;
import util.Reader;
import util.Writter;
import view.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Cette classe principale sert à démarrer l'application
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class MainApp extends Application {

	/**
	 * La fenetre principale
	*/
    private Stage primaryStage;
	/**
	 * L'élément racine
	*/
    private BorderPane rootLayout;
    
	/**
	 * Liste d'emprunts
	*/    
    private ObservableList<Emprunt> ListeEmprunt = FXCollections.observableArrayList();
    
    /**
     * Liste de matériels
     */
    private ObservableList<Materiel> ListeMateriel = FXCollections.observableArrayList();
    
    /**
     * Liste des utilisateurs
     */
    private ObservableList<Utilisateur> ListeUtilisateur = FXCollections.observableArrayList();

    /**
     * Liste des stocks
     */
    private ObservableList<Stock> ListeStock = FXCollections.observableArrayList();
    
    /**
     * Liste des institutions
     */
    private ObservableList<Institution> ListeInstitution = FXCollections.observableArrayList();
    
    /**
     * Liste des lieux
     */
    private ObservableList<Lieu> ListeLieu = FXCollections.observableArrayList();
    
    /**
	 * Le constructeur de la classe de l'application main
	 * 
     * @throws ParseException Erreur lors de l'analyse et conversion d'une variable
     */
    public MainApp() throws ParseException {
    	load();
    }
    
    /**
     * @return La liste des emprunts
     */
    public ObservableList<Emprunt> getListeEmprunt() {
        return this.ListeEmprunt;
    }
    
    /**
     * @return La liste des matériels
     */
    public ObservableList<Materiel> getListeMateriel() {
        return this.ListeMateriel;
    }
    
    /**
     * @return La liste des utilisateurs
     */
    public ObservableList<Utilisateur> getListeUtilisateurs() {
        return this.ListeUtilisateur;
    }
    
    /**
	 * @return the listeStock
	 */
	public ObservableList<Stock> getListeStock() {
		return ListeStock;
	}
	
	/**
	 * @return the listeInstitution
	 */
	public ObservableList<Institution> getListeInstitution() {
		return ListeInstitution;
	}
	

	/**
	 * @return the listeLieu
	 */
	public ObservableList<Lieu> getListeLieu() {
		return ListeLieu;
	}

	/**
     * Méthode permettant de retourner l'indice dans le tableau d'un matériel passé en paramètre
     * @param  m Matériel à rechercher
     * @return index L'indice du matériel à rechercher
     */
    public int rechercheMateriel(Materiel m) {
    	int index = 0;
    	while(index<this.getListeMateriel().size()) {
    		if(m.equals(this.getListeMateriel().get(index))) break;
    		else index++;
    	}
    	return index;
    }
    
	/**
     * Méthode vérifiant que la référence n'est pas déjà affectée à un matériel
     * @param  ref Référence
     * @return trouve True si la référence existe, False sinon
     */
    /*public boolean rechercheMateriel(String ref) {
    	int index = 0;
    	boolean trouve=false;
    	while(index<this.getListeMateriel().size()-1 && !trouve) {
    		if(index<=this.getListeMateriel().size()-2)
    			if((this.getListeMateriel().get(index).getRef().equals(ref))) trouve=true;
    		else index++;
    	}
    	return trouve;
    }*/
    
    /**
     * Méthode permettant de retourner l'indice dans le tableau d'un emprunt passé en paramètre
     * @param  e Emprunt à rechercher
     * @return index L'indice de l'emprunt à rechercher
     */
    public int rechercheEmprunt(Emprunt e) {
    	int index = 0;
    	if(e.getMat()!=null) {
        	while(index<this.getListeEmprunt().size()) {
        		if(e.equals(this.getListeEmprunt().get(index))) break;
        		else index++;
        	}
        	return index;
    	}
    	else return -1;
    }
    
	/**
     * Méthode permettant de retourner l'indice dans le tableau d'un utilisateur passé en paramètre
     * @param  util Utilisateur à rechercher
     * @return index L'indice de l'utilisateur à rechercher
     */
    public int rechercheUtilisateur(Utilisateur util) {
    	int index = 0;
    	while(index<this.getListeUtilisateurs().size()) {
    		if(util.equals(this.getListeUtilisateurs().get(index))) break;
    		else index++;
    	}
    	return index;
    }
    
	/**
     * Méthode permettant de retourner l'indice dans le tableau un lieu passé en paramètre
     * @param  lieu Lieu à rechercher
     * @return index L'indice du lieu
     */
    public int rechercheLieu(Lieu lieu) {
    	int index = 0;
    	while(index<this.getListeLieu().size()) {
    		if(lieu.equals(this.getListeLieu().get(index))) break;
    		else index++;
    	}
    	return index;
    }
    
    
    @Override
	/**
	 * Cette méthode permet de démarrer la fenêtre principale
	 * 
	 * @param primaryStage Le fenetre principale
	 */
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Gestionnaire d'emprunt");

        // Set the application icon.
        this.primaryStage.getIcons().add(new Image("file:ressources/images/icone-32.png"));
        
        initRootLayout();

        interfaceAfficherListEmprunt();
        
        //Fermeture de l'API avec message de confirmation
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
        	    Alert alert = new Alert(AlertType.CONFIRMATION);
        	    alert.setTitle("Confirmation");
        	    alert.setHeaderText("Êtes-vous sûr de vouloir quitter l'application ?");
        	    Optional<ButtonType> option = alert.showAndWait();
        	    
        	    if (option.get() == ButtonType.OK) {
        	    	save();
                	((Stage)event.getSource()).close();
        	    }
        	    else
                    event.consume();
            }
        });
        
    }
    
    /**
     * Initialisation de l'élement racine
     */
    public void initRootLayout() {
        try {
            // Charger l'élément racine depuis le fichier fxml
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            
            // Afficher la scene contenant l'élément racine
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            //primaryStage.setMaximized(true);
            
            // Donne l'accès au controleur ROotLayoutController à l'application main
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);
            
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Afficher la fenetre de liste des emprunts à l'intérieur de l'élément racine
     */
    public void interfaceAfficherListEmprunt() {
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/ListeEmpruntview.fxml"));
            AnchorPane flisteEmprunt = (AnchorPane) loader.load();

            // Position de l'élement flisteEmprunt au centre de la racine
            rootLayout.setCenter(flisteEmprunt);
            
            // Donne l'accès au controleur ListeEmprunt à l'application main
            ListeEmpruntController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Methode permettant d'ouvrir le dialogue ajout d'emprunt et d'enregistrer l'emprunt.
     *
     *	@param emprunt L'emprunt à ajouter 
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceEditerEmprunt(Emprunt emprunt) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/AjoutEmpruntview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ajouter un emprunt");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            AjoutEmpruntController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setEmprunt(emprunt);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }    
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion d'utilisateur
     *
     * @param disable Boolean indiquant si le Main Stage est desactive ou non
     * @return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceAfficherListUtilisateur(boolean disable) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/ListeUtilisateurview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Liste des utilisateurs");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ListeUtilisateurController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this,disable);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion d'un utilisateur
     *
     *	@param utilisateur L'utilisateur à gérer
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceGestionUtilisateur(Utilisateur utilisateur) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/AjoutUtilisateurview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ajouter un utilisateur");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            dialogStage.setResizable(false);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            AjoutUtilisateurController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setUtilisateur(utilisateur);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    } 
    
    /**
     * Methode permettant d'ouvrir le dialogue gestion d'un matériel
     *
     * @param disable Boolean indiquant si le Main Stage est desactive ou non
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceAfficherListeMateriel(boolean disable) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/ListeMaterielview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Liste des materiel");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ListeMaterielController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this,disable);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion d'un matériel
     *
     *	@param materiel Le matériel à gérer
     * 	@return Vrai si l'utilisateur a cliqué sur OK, Faux sinon
     */
    public boolean interfaceGestionMateriel(Materiel materiel) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/AjoutMaterielview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ajouter un matériel");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            dialogStage.setResizable(false);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            AjoutMaterielController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setMateriel(materiel);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    } 
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion de stock
     *
     *	@param disable Boolean indiquant si le Main Stage est desactive ou non
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceAfficherListeStock(boolean disable) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/ListeStockview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Liste des stocks");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ListeStockController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this,disable);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    } 
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion d'un stock
     *
     *	@param stock Le stock à gérer
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceGestionStock(Stock stock) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/AjoutStockview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ajouter un stock");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            dialogStage.setResizable(false);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            AjoutStockController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setStock(stock);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    } 
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion d'institution
     *
     *	@param disable Indique si le Main Stage est desactive
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceAfficherListeInstitution(boolean disable) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/ListeInstitutionview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Liste des institutions");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ListeInstitutionController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this,disable);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion d'une institution
     *
     *	@param institution L'institution à gérer
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceGestionInstitution(Institution institution) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/AjoutInstitutionview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ajouter une institution");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            dialogStage.setResizable(false);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            AjoutInstitutionController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setInstitution(institution);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    } 
    
    /**
     * Methode permettant d'ouvrir le dialogue gestion d'un lieu
     *    	Institution inst1 = new Institution("ENSIIE", "1 Square de la Résistance");
     *  
     *  @param disable Indique si la gestion d'un lieu doit être desactive
     * 	@return vrai si l'utilisateur a cliqué sur OK, faux sinon
     */
    public boolean interfaceAfficherListeLieu(boolean disable) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/ListeLieuview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Liste des lieux");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ListeLieuController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this,disable);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Methode permettant d'ouvrir le dialogue de gestion d'un stock
     *
     *	@param lieu Le lieu à éditer
     *	@return True si l'utilisateur a clique sur OK, False sinon
     */
    public boolean interfaceGestionLieu(Lieu lieu) {
        try {
            // Charge le fichier fxml 
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/AjoutLieuview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Crée la fenêtre dialogue
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ajouter un lieu");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            dialogStage.setResizable(false);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            AjoutLieuController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setLieu(lieu);

            // Affiche la fenetre dialogue jusqu'a ce qu'elle soit fermée 
            dialogStage.showAndWait();

            return controller.estOkclique();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    } 
    
    /**
     * Retourne la fenêtre principale
     * @return primaryStage 
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
    
/*============================================================================*/
    
    /**
     * Charge les donnees utilisateur depuis les fichiers specifies. La liste d'utilisateur
     * sera remplacee.
     */
    public void loadUtilisateurs() {
        try {
            JAXBContext contextEtudiant = JAXBContext.newInstance(EtudiantListWrapper.class);
            JAXBContext contextEnseigant = JAXBContext.newInstance(EnseignantListWrapper.class);
            JAXBContext contextStartup = JAXBContext.newInstance(StartupListWrapper.class);
            
            Unmarshaller uEtudiant = contextEtudiant.createUnmarshaller();
            Unmarshaller uEnseignant = contextEnseigant.createUnmarshaller();
            Unmarshaller uStartup = contextStartup.createUnmarshaller();
            
            // Reading XML from the file and unmarshalling.
            EtudiantListWrapper wEtudiant = (EtudiantListWrapper) uEtudiant.unmarshal(new File(MainApp.class.getClassLoader().getResource("sauvegardes/etudiants.xml").getPath()));
            EnseignantListWrapper wEnseignant = (EnseignantListWrapper) uEnseignant.unmarshal(new File(MainApp.class.getClassLoader().getResource("sauvegardes/enseignants.xml").getPath()));
            StartupListWrapper wStartup = (StartupListWrapper) uStartup.unmarshal(new File(MainApp.class.getClassLoader().getResource("sauvegardes/startups.xml").getPath()));
            
            ListeUtilisateur.clear();
            ListeUtilisateur.addAll(wEtudiant.getEtudiants());
            ListeUtilisateur.addAll(wEnseignant.getEnseignants());
            ListeUtilisateur.addAll(wStartup.getStartups());

        } catch (Exception e) { // catches ANY exception
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not save data");
            alert.setContentText("Could not load data from file:\n Exception : " + e.getMessage() + "\n Cause : " + e.getCause());

            alert.showAndWait();
        }
    }

    /**
     * Sauvegarde les données utilisateur actuelles dans les fichiers sprecifies.
     * 
     */
    public void saveUtilisateurs() {
    	ObservableList<Etudiant> ListeEtudiant = FXCollections.observableArrayList();
    	ObservableList<Enseignant> ListeEnseignant = FXCollections.observableArrayList();
    	ObservableList<Startup> ListeStartup = FXCollections.observableArrayList();
    	
    	for (Utilisateur user : ListeUtilisateur) {
    		if (user instanceof Etudiant) ListeEtudiant.add((Etudiant) user);
    		if (user instanceof Enseignant) ListeEnseignant.add((Enseignant) user);
    		if (user instanceof Startup) ListeStartup.add((Startup) user);
    	}
    	
        try {
            JAXBContext contextEtudiant = JAXBContext.newInstance(EtudiantListWrapper.class);
            JAXBContext contextEnseigant = JAXBContext.newInstance(EnseignantListWrapper.class);
            JAXBContext contextStartup = JAXBContext.newInstance(StartupListWrapper.class);
            
            Marshaller mEtudiant = contextEtudiant.createMarshaller();
            Marshaller mEnseigant = contextEnseigant.createMarshaller();
            Marshaller mStartup = contextStartup.createMarshaller();
            mEtudiant.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            mEnseigant.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            mStartup.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Wrapping our emprunt data.
            EtudiantListWrapper wrapperEtudiant = new EtudiantListWrapper();
            wrapperEtudiant.setEtudiants(ListeEtudiant);
            EnseignantListWrapper wrapperEnseigant = new EnseignantListWrapper();
            wrapperEnseigant.setEnseignants(ListeEnseignant);
            StartupListWrapper wrapperStartup = new StartupListWrapper();
            wrapperStartup.setStartups(ListeStartup);

            // Marshalling and saving XML to the file.
            mEtudiant.marshal(wrapperEtudiant, new File(MainApp.class.getClassLoader().getResource("sauvegardes/etudiants.xml").getPath()));
            mEnseigant.marshal(wrapperEnseigant, new File(MainApp.class.getClassLoader().getResource("sauvegardes/enseignants.xml").getPath()));
            mStartup.marshal(wrapperStartup, new File(MainApp.class.getClassLoader().getResource("sauvegardes/startups.xml").getPath()));

            System.out.println("Utilisateurs sauvegardés !");
        } catch (Exception e) { // catches ANY exception
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not save data");
            alert.setContentText("Could not save data to file:\n Exception : " + e.getMessage() + "\n Cause : " + e.getCause());

            alert.showAndWait();
        }
    }
    
    /**
     * Sauvegarde toutes les donnees de l'application dans le XML
     */
    public void save() {
    	saveUtilisateurs();
    	Writter.materiel(ListeMateriel);
    	Writter.emprunt(ListeEmprunt, ListeUtilisateur, ListeMateriel);
    	Writter.emplacement(ListeStock, ListeLieu, ListeInstitution);
    }
    
    /**
     * Charge toutes les donnees de l'application depuis le XML
     */
    private void load() {
    	loadUtilisateurs();
    	Reader.materiel(ListeMateriel);
    	Reader.emprunt(ListeEmprunt, ListeUtilisateur, ListeMateriel);
    	Reader.emplacement(ListeStock, ListeLieu, ListeInstitution, ListeMateriel, ListeUtilisateur);
    }
}
