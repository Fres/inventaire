package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import controller.MainApp;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;

/**
 * Cette classe est utilisée pour ajouter ou modifier un lieu dans la fenetre AjoutEmprunt
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class AjoutLieuController {

	/**
	 * Champs représentant le numéro d'emprunt
	 */
    @FXML
    private TextField nom_lieu;
           
    /**
     * Champs représentant le nom de l'institution
     */
    @FXML
    private Label nom_insti;
    
    
    private Stage dialogStage;
    private Lieu lieu;
    private boolean okClick = false;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;

    /**
     * Méthode initialisant le contrôleur de la classe AjoutLIeuController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'afficher les détails d'un lieu
     *
     * @param lieu Lieu
     */
    public void setLieu(Lieu lieu) {
 
        this.lieu = lieu;

        if(lieu.getNom_lieu()!=null) {
        	nom_lieu.setText(lieu.getNom_lieu());
        }
    	if(lieu.getInsti()!=null) {
    	    nom_insti.setText(lieu.getInsti().getName());
    	}

    }

    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }

    /**
     * Méthode appelé lorsque l'utilisateur clique sur OK pour valider le lieu
     */
    @FXML
    private void gestionOk() {
        if (isInputValid()) {
        	
        	this.lieu.setNom_lieu(nom_lieu.getText());
    		okClick = true;
            dialogStage.close();
        }
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un lieu
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
    
    /**
     * Méthode permettant d'effacer les saisies
     */
    @FXML
    private void gestionEffacer() {

    }
        
    /**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Choisir une institution.."
	 * Elle ouvre une fenetre permettant d'ajouter une institution
     */
    @FXML
	private void ajouterInstitution() {
	    boolean okClick = mainApp.interfaceAfficherListeInstitution(false);
	    if (okClick) {
	    	int index_last = mainApp.getListeLieu().size() - 1;
	    	lieu = mainApp.getListeLieu().get(index_last);
	        this.setLieu(lieu);
	    }
	}


    /**
     * Méthode permettant de gérer les saisies au clavier de l'utilisateurs
     *
     * @return Vrai si l'utilisation a bien saisie les champs, faux sinon
     */
    private boolean isInputValid() {
        String errorMessage = "";
        
        if(nom_insti.getText().trim().isEmpty()) {
            errorMessage += "Veuillez choisir une institution\n";
        }
        if(nom_lieu.getText().trim().isEmpty()) 
        	errorMessage+="Veuillez rentrer un nom de lieu! \n";
        else if(nom_lieu.getText().matches("[0-9]*")) {
            errorMessage += "Veuillez rentrer un nom de lieu valide!\n";
     	}
        	

        if (errorMessage.length() == 0) return true;
        else {
            //Afficher un message d'erreur
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalides");
            alert.setHeaderText("Veuillez corriger les champs invalides");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
    
    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        
    }
}
