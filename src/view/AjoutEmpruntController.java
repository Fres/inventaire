package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import controller.MainApp;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;

/**
 * Cette classe est utilisée pour ajouter ou modifier un emprunt dans la fenetre AjoutEmprunt
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class AjoutEmpruntController {

	/**
	 * Champs représentant le numéro d'emprunt
	 */
    @FXML
    private Label num_emprunt;
    
	/**
	 * Champs représentant date de l'emprunt
	 */
    @FXML
    private DatePicker dateEmprunt;
    
	/**
	 * Champs représentant la date de retour
	 */
    @FXML
    private DatePicker dateRetour;
    
    /**
     * Champ représentant le nom de l'emprunteur
     */
    @FXML
    private Label nom_user;
    
    /**
     * Champs représentant le prénom de l'emprunteur
     */
    @FXML
    private Label prenom_user;
    
    /**
     * Champs représentant la fonction de l'utilisateur
     */
    @FXML
    private Label fonction_user;

	/**
	 * Table représentant la liste des matériels
	*/
	@FXML
    private TableView<Materiel> materielTable;
	
	/**
	 * Colonne du tableau représentant la référence du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> ref;
    
	/**
	 * Colonne du tableau représentant le nom du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> nom_materiel;
    
	/**
	 * Colonne du tableau représentant le marque du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> marque_materiel;
    
	/**
	 * Colonne du tableau représentant l'état du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> etat_materiel;
    
    
    private Stage dialogStage;
    private Emprunt emprunt;
    private boolean okClick = false;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;

    /**
     * Méthode initialisant le contrôleur de la classe AjoutEmpruntController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {
		
        // Initialise la table de matériel avec les quatre colonnes
    	
        ref.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRef()));
        nom_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        marque_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMarque()));
        etat_materiel.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getEtat()));
        
        //Ne pas permettre de choisir les dates passées pour la date d'emprunt
        Callback<DatePicker, DateCell> callEmprunt = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker param) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        LocalDate today = LocalDate.now();
                        setDisable(empty || item.compareTo(today) < 0);
                    }
                };
            }
        };
        
        //Ne pas permettre de choisir une date inférieure à la date d'emprunt pour la date de retour
        Callback<DatePicker, DateCell> callRetour = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker param) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        setDisable(empty || item.compareTo(dateEmprunt.getValue()) < 0);
                    }
                };
            }
        };
        dateRetour.setDayCellFactory(callRetour);
        dateEmprunt.setDayCellFactory(callEmprunt);

    
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'afficher les détails d'un emprunt
     *
     * @param emprunt L'emprunt
     */
    public void setEmprunt(Emprunt emprunt) {
 
        this.emprunt = emprunt;

    	if(emprunt.getMat()!=null) {
    	    materielTable.getItems().add(emprunt.getMat());
    	    materielTable.getSelectionModel().select(emprunt.getMat());
    	}
    	if(emprunt.getUtilisateur()!=null) {
            nom_user.setText(emprunt.getUtilisateur().getNom());
            prenom_user.setText(emprunt.getUtilisateur().getPrenom());
            fonction_user.setText(emprunt.getUtilisateur().getClass().getSimpleName());
    	}
    	if(emprunt.getDateEmprunt()!=null){
            LocalDate datemprunt = emprunt.getDateEmprunt().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            dateEmprunt.setValue(datemprunt);
            LocalDate dateretour = emprunt.getDateRetour().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            dateRetour.setValue(dateretour);
    	}else {
            LocalDate datemprunt = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            dateEmprunt.setValue(datemprunt);
    	}
    		

    }

    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }

    /**
     * Méthode appelé lorsque l'utilisateur clique sur OK pour valider l'emprunt
     */
    @FXML
    private void gestionOk() {
        if (isInputValid()) {
        	
        	int index_emp,index_mat;
    	       	    
    	    //Retourne l'indice de l'emprunt dans la liste des emprunt
        	index_emp = mainApp.rechercheEmprunt(emprunt);
        	
        	//Récupère le matériel selectionné dans la fenêtre
        	Materiel m = materielTable.getSelectionModel().getSelectedItem();
        	//Retourne l'indice de l'emprunt dans la liste des emprunt
        	index_emp = mainApp.rechercheEmprunt(emprunt);
        	//Si c'est un nouveau emprunt, on attribue le matériel selectionné
        	if(index_emp==-1) emprunt.setMat(m);
        	//Sinon
        	else {
        		//Récupère l'indice de l'ancien matériel de l'emprunt dans la liste des matériels
            	index_mat = mainApp.rechercheMateriel(emprunt.getMat());
            	//Retire l'emprunt auquel est affecté l'ancien matériel et rend disponible l'ancien matériel
            	mainApp.getListeMateriel().get(index_mat).retirerEmprunter(emprunt);
            	//Ajout du nouveau matériel à l'emprunt
        		mainApp.getListeEmprunt().get(index_emp).setMat(m);
        	}
        	 
        	//Récupère l'indice du nouveau matériel dans la liste des matériels
        	index_mat = mainApp.rechercheMateriel(m);
        	//Rend indisponible le matériel et l'affecte emprunt
        	mainApp.getListeMateriel().get(index_mat).estEmprunter(emprunt);
        	
        	//Attribution des dates d'emprunt et de retour à l'emprunt
    	    emprunt.setDateEmprunt(Date.from(dateEmprunt.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
    	    emprunt.setDateRetour(Date.from(dateRetour.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        		    
    		okClick = true;
            dialogStage.close();
        }
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un emprunt
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
        
    /**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Choisir un emprunteur..."
	 * Elle ouvre une fenetre permettant d'ajouter un enmprunteur
     */
    @FXML
	private void ajouterEmprunteur() {
	    boolean okClick = mainApp.interfaceAfficherListUtilisateur(false);
	    if (okClick) {
	    	int index_last = mainApp.getListeEmprunt().size() - 1;
	    	emprunt = mainApp.getListeEmprunt().get(index_last);
	        this.setEmprunt(emprunt);
	    }
	}
    
    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        try {
          dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
          return false;
        }
        return true;
      }

    /**
     * Méthode permettant de gérer les saisies au clavier de l'utilisateurs
     *
     * @return Vrai si l'utilisation a bien saisie les champs, faux sinon
     */
    private boolean isInputValid() {
        String errorMessage = "";

        /*if (firstNameField.getText() == null || firstNameField.getText().length() == 0) {
            errorMessage += "No valid first name!\n";
        }*/

        /*if (postalCodeField.getText() == null || postalCodeField.getText().length() == 0) {
            errorMessage += "No valid postal code!\n";
        } else {
            // try to parse the postal code into an int.
            try {
                Integer.parseInt(postalCodeField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid postal code (must be an integer)!\n";
            }
        }*/

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        if (dateEmprunt.getValue()==null) {
            errorMessage += "Date d'emprunt non valide!\n";
        }else {
            /*if (isValidDate(dateEmprunt.getValue().format(formatter))) {
                errorMessage += "Date d'emprunt non valide!. Utilisez le format jj/mm/aaaa!\n";
            }*/
        }
        
        if (dateRetour.getValue()==null) {
            errorMessage += "Date de retour non valide!\n";
        }else {
            /*if (isValidDate(dateRetour.getValue().format(formatter))) {
                errorMessage += "Date de retour non valide!. Utilisez le format jj/mm/aaaa!\n";
            }*/
        }
        
        if(materielTable.getSelectionModel().getSelectedItem()==null) {
            errorMessage += "Veuillez selectionner le matériel emprunté\n";
        }
        
        if(nom_user.getText().trim().isEmpty()) {
            errorMessage += "Veuillez selectionner l'emprunteur\n";
        }
        	

        if (errorMessage.length() == 0) return true;
        else {
            //Afficher un message d'erreur
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalides");
            alert.setHeaderText("Veuillez corriger les champs invalides");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
    
    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Ajout des matériels disponible dans la table view des matériels
    	for (int i = 0; i <this.mainApp.getListeMateriel().size(); i++) {
    		if(this.mainApp.getListeMateriel().get(i).estDispo())
    			materielTable.getItems().add(this.mainApp.getListeMateriel().get(i));
		}
        
    }
}
