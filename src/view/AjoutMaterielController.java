package view;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import controller.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;

/**
 * Cette classe est utilisée pour ajouter ou modifier un matériel dans la fenetre AjoutMateriel
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class AjoutMaterielController {

	/**
	 * Champs représentant la référence du matériel
	 */
    @FXML
    private TextField ref_mat;
    
	/**
	 * Champs représentant le nom du matériel
	 */
    @FXML
    private TextField nom_mat;
    
	/**
	 * Champs représentant la marque du matériel
	 */
    @FXML
    private TextField marque_mat;
    
	/**
	 * Champs représentant le prix d'achat du matériel
	 */
    @FXML
    private TextField prix_mat;
    
	/**
	 * Champs représentant la date d'achat du matériel
	 */
    @FXML
    private DatePicker date_achat_mat;
    
    /**
     * Choice box représentant les états du matérriels
     */
    @FXML
    private ChoiceBox<String> etat_materiel;
    
    /**
     * Champs représentant les types d'appareils
     */
    @FXML
    private ChoiceBox<String> appareil;
    
    /**
     * Champs représentant les types d'appareils
     */
    @FXML
    private Label label_appareil;
 
    /**
     * Champs représentant les types d'appareils
     */
    @FXML
    private ChoiceBox<String> entree;
    
    /**
     * Champs représentant les types d'appareils
     */
    @FXML
    private Label label_entree;
    
    /**
     * Choice box représentant les types d'appareils
     */
    @FXML
    private ChoiceBox<String> sortie;
    
    /**
     * Champs représentant les types d'appareils
     */
    @FXML
    private Label label_sortie;
    
    /**
     * Choice box représentant les OS des apapreils
     */
    @FXML
    private ChoiceBox<String> os_appareil;
    
    /**
     * Champs représentant les OS des appareils
     */
    @FXML
    private Label label_os_appareil;
    
    /**
     * Champs représentant la taille en pixel de l'appareil
     */
    @FXML
    private TextField taille_appareil;
    
    /**
     * Champs représentant les types d'appareils
     */
    @FXML
    private Label label_taille_appareil;
    
    /**
     * Choice box représentant la résolution de l'appareil
     */
    @FXML
    private ChoiceBox<String> reso_appareil;
    
    /**
     * Choice box représentant la résolution de l'appareil
     */
    @FXML
    private Label label_reso_appareil;
    
    /**
     * Champs représentant la sensibilité du péripherique d'entrée
     */
    @FXML
    private Spinner<Integer> sensibilite_entree;
    
    /**
     * Champs représentant la sensibilité du péripherique d'entrée
     */
    @FXML
    private Label label_sensibilite_entree;

    /**
     * Choice box représentant les types de connectique du périphérique d'entrée
     */
    @FXML
    private ChoiceBox<String> connectique_entree;
    
    /**
     * Choice box représentant les types de connectique du périphérique d'entrée
     */
    @FXML
    private Label label_connectique_entree;
    
	/**
	 * Choice box représentant la qualité de sortie du périphérique de sortie
	 */
    @FXML
    private ChoiceBox<String> qualite_sortie;
    
	/**
	 * Choice box représentant la qualité de sortie du périphérique de sortie
	 */
    @FXML
    private Label label_qualite_sortie;
    
    
    private Stage dialogStage;
    private Materiel materiel;
    private boolean okClick = false;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;

    /**
     * Méthode initialisant le contrôleur de la classe AjoutMaterielController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {
    	
        //Ne pas permettre de choisir les dates passées pour la date d'achat
        Callback<DatePicker, DateCell> callMateriel = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker param) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        LocalDate today = LocalDate.now();
                        setDisable(empty || item.compareTo(today) < 0);
                    }
                };
            }
        };
        date_achat_mat.setDayCellFactory(callMateriel);
        
    	//Ajout des types d'états dans la choicebox
    	for(int i=0;i< model.Materiel.Etat.values().length;i++) {
            etat_materiel.getItems().add(model.Materiel.Etat.values()[i].toString());
    	}

        
        //Ajout des types d'appareils dans la choicebox
        appareil.getItems().add("Telephone");
        appareil.getItems().add("Tablette");
        
        //Ajout des types de périphériques entrées dans la choicebox
        entree.getItems().add("Souris");
        entree.getItems().add("Capteur");
        entree.getItems().add("ManetteJeu");
        entree.getItems().add("Webcam");
        entree.getItems().add("ManetteCasque");
        
        //Ajout des types d'OS dans la choicebox
    	for(int i=0;i< model.Appareil.OS.values().length;i++) {
            os_appareil.getItems().add(model.Appareil.OS.values()[i].toString());
    	};
    	
    	//Ajout des différentes résolutions 
    	for(int i=0;i< model.Appareil.Res.values().length;i++) {
            reso_appareil.getItems().add(model.Appareil.Res.values()[i].toString());
    	};
    	
    	//Initialisation du  spinner pour la sensibilité
    	int initialValue=0;
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, initialValue);
        sensibilite_entree.setValueFactory(valueFactory);
        
        //Ajout des type des connectiques dans la choicebox
    	for(int i=0;i< model.PeripheriqueEntree.connect.values().length;i++) {
            os_appareil.getItems().add(model.PeripheriqueEntree.connect.values()[i].toString());
    	};
        
       //Ajout des types de périphériques sortie dans la choicebox
        sortie.getItems().add("CasqueAudio");
        sortie.getItems().add("CasqueVR");
        
        //Ajout des différentes qualités sonores dans la choicebox
    	for(int i=0;i< model.PeripheriqueSortie.QUALITE.values().length;i++) {
            qualite_sortie.getItems().add(model.PeripheriqueSortie.QUALITE.values()[i].toString());
    	};
    	
        //Ajout des différentes connectiques
    	for(int i=0;i< model.PeripheriqueEntree.connect.values().length;i++) {
            connectique_entree.getItems().add(model.PeripheriqueEntree.connect.values()[i].toString());
    	};

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'afficher les détails d'un matériel
     *
     * @param materiel Le matériel
     */
    public void setMateriel(Materiel materiel){
 
    	this.materiel = materiel;

        if(materiel.getRef()!=null)
            ref_mat.setText(materiel.getRef());
        if(materiel.getNom()!=null)
            nom_mat.setText(materiel.getNom());
        if(materiel.getMarque()!=null)
            marque_mat.setText(materiel.getMarque());
        if(Double.toString(materiel.getPrix())!=null)
            prix_mat.setText(Double.toString(materiel.getPrix()));
    	if(materiel.getDateAchat()!=null){
            LocalDate achat = materiel.getDateAchat().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            date_achat_mat.setValue(achat);
    	}else {
            LocalDate achat = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            date_achat_mat.setValue(achat);
    	}
    	if(materiel.getEtat()!=null) {
    		etat_materiel.setValue(materiel.getEtat().toString());
    	}
                
        if(materiel.getClass().getSimpleName().contentEquals("Appareil") || materiel.getClass().getSuperclass().getSimpleName().equals("Appareil")) {
        	this.appareil.setValue(materiel.getClass().getSimpleName());
          	if(this.os_appareil.getValue()!=null) this.os_appareil.setValue(((Appareil) materiel).getOs().toString());
          	if(this.taille_appareil.getText()!=null)this.taille_appareil.setText(Double.toString(((Appareil) materiel).getTaille()));
          	if(this.reso_appareil.getValue()!=null)this.reso_appareil.setValue(((Appareil) materiel).getResolution().toString());
        	this.entree.setVisible(false);
        	this.sortie.setVisible(false);
        	this.sensibilite_entree.setVisible(false);
        	this.connectique_entree.setVisible(false);
        	this.qualite_sortie.setVisible(false);
        	this.label_entree.setVisible(false);
        	this.label_sortie.setVisible(false);
        	this.label_sensibilite_entree.setVisible(false);
        	this.label_connectique_entree.setVisible(false);
        	this.label_qualite_sortie.setVisible(false);
        	
        }
        else if(materiel.getClass().getSimpleName().contentEquals("PeripheriqueEntree") ||materiel.getClass().getSuperclass().getSimpleName().equals("PeripheriqueEntree")) {
        	this.entree.setValue(materiel.getClass().getSimpleName());
        	SpinnerValueFactory<Integer> sens = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, ((PeripheriqueEntree) materiel).getSensibilite());
        	if(this.sensibilite_entree.getValue()!=null)this.sensibilite_entree.setValueFactory(sens);
        	if(this.connectique_entree.getValue()!=null)this.connectique_entree.setValue((((PeripheriqueEntree) materiel).getConnectique().toString()));
        	this.sortie.setVisible(false);
        	this.appareil.setVisible(false);
        	this.qualite_sortie.setVisible(false);
        	this.os_appareil.setVisible(false);
        	this.taille_appareil.setVisible(false);
        	this.reso_appareil.setVisible(false);
        	this.label_sortie.setVisible(false);
        	this.label_appareil.setVisible(false);
        	this.label_qualite_sortie.setVisible(false);
        	this.label_os_appareil.setVisible(false);
        	this.label_taille_appareil.setVisible(false);
        	this.label_reso_appareil.setVisible(false);
        }
        else if(materiel.getClass().getSimpleName().contentEquals("PeripheriqueSortie") ||materiel.getClass().getSuperclass().getSimpleName().equals("PeripheriqueSortie")) {
        	this.sortie.setValue(materiel.getClass().getSimpleName());
        	if(this.qualite_sortie.getValue()!=null)this.qualite_sortie.setValue((((PeripheriqueSortie) materiel).getQualite().toString()));
        	this.os_appareil.setVisible(false);
        	this.taille_appareil.setVisible(false);
        	this.reso_appareil.setVisible(false);
        	this.entree.setVisible(false);
        	this.appareil.setVisible(false);
        	this.sensibilite_entree.setVisible(false);
        	this.connectique_entree.setVisible(false);
        	this.label_os_appareil.setVisible(false);
        	this.label_taille_appareil.setVisible(false);
        	this.label_reso_appareil.setVisible(false);
        	this.label_entree.setVisible(false);
        	this.label_appareil.setVisible(false);
        	this.label_sensibilite_entree.setVisible(false);
        	this.label_connectique_entree.setVisible(false);
        }
        	
    }
    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }

    /**
     * Méthode appelé lorsque l'utilisateur clique sur OK pour valider le matériel
     */
    @FXML
    private void gestionOk() {
        if (isInputValid()) {

            materiel.setRef(ref_mat.getText());
            materiel.setNom(nom_mat.getText());
            materiel.setMarque(marque_mat.getText());
            materiel.setPrix(Double.parseDouble(prix_mat.getText()));
    	    materiel.setDateAchat(Date.from(date_achat_mat.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        	materiel.setEtat(model.Materiel.Etat.valueOf(etat_materiel.getValue()));
        	
                    
            if(materiel.getClass().getSuperclass().getSimpleName().equals("Appareil")) {
            	((Appareil) materiel).setOs(os_appareil.getValue() !=null ? model.Appareil.OS.valueOf(os_appareil.getValue()) : null);
            	((Appareil) materiel).setTaille(taille_appareil.getText() !=null ? Double.parseDouble(taille_appareil.getText()) : null);
            	((Appareil) materiel).setResolution(reso_appareil.getValue()!=null ? model.Appareil.Res.valueOf(reso_appareil.getValue()) : null);
            }
            else if(materiel.getClass().getSuperclass().getSimpleName().equals("PeripheriqueEntree")) {
            	((PeripheriqueEntree) materiel).setSensibilite(sensibilite_entree.getValue() !=null ? Integer.parseInt(sensibilite_entree.getValue().toString()) : null);
            	((PeripheriqueEntree) materiel).setConnectique(connectique_entree.getValue() !=null ? model.PeripheriqueEntree.connect.valueOf(connectique_entree.getValue().toString()) : null);
            }
            else if(materiel.getClass().getSuperclass().getSimpleName().equals("PeripheriqueSortie")) {
            	((PeripheriqueSortie) materiel).setQualite(qualite_sortie.getValue() != null ? model.PeripheriqueSortie.QUALITE.valueOf(qualite_sortie.getValue().toString()) : null);
            }    
          
    		okClick = true;
            dialogStage.close();
        }
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un materiel
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
    
    /**
     * Méthode permettant d'effacer les saisies
     */
    @FXML
    private void gestionEffacer() {
    	if(materiel!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Effacer tous les champs");
		    alert.setHeaderText("Voulez-vous vraiment effacer tous les champs");
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
		        this.ref_mat.setText(null);
		        this.nom_mat.setText(null);
		        this.marque_mat.setText(null);
		        this.prix_mat.setText(null);
		        this.etat_materiel.setValue(null);
		        this.appareil.setValue(null);
		        this.entree.setValue(null);
		        this.sortie.setValue(null);
		        this.os_appareil.setValue(null);
		        this.taille_appareil.setText(null);
		        this.reso_appareil.setValue(null);
		        this.sensibilite_entree.setValueFactory(null);
		        this.connectique_entree.setValue(null);
		        this.qualite_sortie.setValue(null);
		    } 
    	}

    }
    
    /**
     * Méthode permettant de gérer les saisies au clavier de l'utilisateurs
     *
     * @return Vrai si l'utilisation a bien saisie les champs, faux sinon
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (ref_mat.getText()==null || ref_mat.getText().isEmpty()) {
            errorMessage += "Veuillez rentrer une référence !\n";
        }
        else if(ref_mat.getText().matches("[0-9]*")) {
            errorMessage += "Veuillez rentrer une référence valide!\n";
        }
        /*else if(mainApp.rechercheMateriel(ref_mat.getText()))
            errorMessage += "La référence du matériel existe déjà!\n";*/
        
        
        if (nom_mat.getText()==null ||nom_mat.getText().isEmpty()) {
            errorMessage += "Veuillez rentrer un nom !\n";
        } 
        else if(nom_mat.getText().matches("[0-9]*")) {
            errorMessage += "Veuillez rentrer un nom valide!\n";
        }
        
        if (marque_mat.getText()==null ||marque_mat.getText().isEmpty()) {
            errorMessage += "Veuillez rentrer une marque !\n";
        } 
        else if(marque_mat.getText().matches("[0-9]*")) {
            errorMessage += "Veuillez rentrer une marque valide!\n";
        }
        
        if (prix_mat.getText() !=null) {
        	if(!prix_mat.getText().matches("[0-9]*.[0-9]*"))
        		errorMessage += "Veuillez rentrer un prix d'achat valide!\n";
        } 

        if (errorMessage.length() == 0) return true;
        else {
            //Afficher un message d'erreur
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalides");
            alert.setHeaderText("Veuillez corriger les champs invalides");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
    
    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
       
    }
}
