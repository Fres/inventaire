/**
 * 
 */
package view;

import java.util.Optional;
import java.util.function.Predicate;

import model.*;
import controller.MainApp;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Cette classe est utilisée pour gérer les données dans le tableau des institutions
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ListeInstitutionController {
	/**
	 * Table représentant la liste des institutions
	*/
	@FXML
    private TableView<Institution> institutionTable;
	
	/**
	 * Colonne du tableau représentant le nom de l'institution
	*/
    @FXML 
    private TableColumn<Institution, String> nom_insti;
    
	/**
	 * Colonne du tableau représentant l'adresse de l'institution
	*/
    @FXML 
    private TableColumn<Institution, String> adresse_insti;
       
	/**
	 * Colonne du tableau représentant le nombre de lieux
	*/
    @FXML
    private TableColumn<Institution, Number> nombre_lieu_insti;
        
    /**
     * Bouton ok
     */
    @FXML
    private Button ok;
    
    /**
     * Bouton annuler
     */
    @FXML
    private Button annuler;
    
    /**
     * Barre de recherche
     */
    @FXML
    private TextField recherche;
       
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;
    
    private Stage dialogStage;
    private boolean okClick = false;

    /**
     * Le constructeur du contrôleur ListeInstitution
     * Le constructeur est appelé avant la méthode initialize
     */
    public ListeInstitutionController() {
    }

    /**
     * Méthode initialisant le contrôleur de la classe ListeInstitutionController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {

        // Initialise la table des utilisateurs
    	
        nom_insti.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        adresse_insti.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAdresse()));
        nombre_lieu_insti.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getListe_l().size()));
                
        //Déclenche la recherche quand il y a une saisie au clavier
        this.recherche.setOnKeyPressed(e -> {recherche(e);});
        
    }
    
    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    
    /**
     * Méthode permettant d'effectuer une recherche sur la table des institutions
     * 
     *@param e Action déclenchée par la saisie du clavier
     */
	public void recherche(KeyEvent e)
	{
		FilteredList<Institution> filterData = new FilteredList<Institution>(mainApp.getListeInstitution());
		this.recherche.textProperty().addListener((observableValue, olValue, newValue) ->{
			filterData.setPredicate((Predicate<? super Institution>) institution-> {
				if(newValue == null || newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter = newValue.toLowerCase();
				
				//Recherche selon la référence du matériel
				int i=0;
				boolean trouve=false;
				while(!trouve && i<institution.getListe_l().size()) {
					if(institution.getListe_l().get(i).getNom_lieu().toLowerCase().contains(newValue.toLowerCase())){
						trouve=true;
						return true;
					}
					i++;
				}
				//Recherche selon le nom du lieu du stock
				if(institution.getName().toLowerCase().contains(newValue.toLowerCase())){
					return true;
				}
					
				
				return false;
			});
		});
		
		SortedList<Institution> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.institutionTable.comparatorProperty());
		this.institutionTable.setItems(sortedData);
	}
	
    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }
    
    /**
     * Méthode appelée lorsque l'utilisateur clique sur OK pour valider l'institution
     */
    @FXML
    private void gestionOk() {

    	int index_last = mainApp.getListeLieu().size() - 1;
        Institution institution = institutionTable.getSelectionModel().getSelectedItem();
        mainApp.getListeLieu().get(index_last).setInsti(institution);
        okClick = true;
        dialogStage.close();
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'une institution
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
	
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..."
	 * Elle ouvre une fenetre permettant d'ajouter une institution
	 */
	@FXML
	private void ajouterInstitution() {
	    Institution insti = new Institution(null,null);
	    mainApp.getListeInstitution().add(insti);
    	int index_last = mainApp.getListeInstitution().size() - 1;
	    boolean okClick = mainApp.interfaceGestionInstitution(mainApp.getListeInstitution().get(index_last));
	    if (okClick)
	        mainApp.getListeInstitution().set(index_last, mainApp.getListeInstitution().get(index_last));
	    else
	    	mainApp.getListeInstitution().remove(index_last);
	}

	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Modifier..."
	 * Elle ouvre une fenetre permettant de modifier une institution
	 */
	@FXML
	private void modifierInstitution() {
	    Institution institution = institutionTable.getSelectionModel().getSelectedItem();
	    if (institution != null) {
	        boolean okClicked = mainApp.interfaceGestionInstitution(institution);
	        if (okClicked) {
	        	int indexSelect = institutionTable.getSelectionModel().getSelectedIndex();
	        	institutionTable.getItems().set(indexSelect, institution);
	        }
	    }else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas d'institution selectionné");
	        alert.setContentText("Veuillez selectionner une institution dans la table.");
	        alert.showAndWait();
	        }
	}
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Supprimer..."
	 * Elle ouvre une fenetre de confirmation
	 */
	@FXML
	private void supprimerInstitution() {
        
	    Institution institution = institutionTable.getSelectionModel().getSelectedItem();
	    if(institution!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Supprimer l'institution");
		    alert.setHeaderText("Confirmez-vous la suppression de l'institution ?");
		    alert.setContentText(institution.getName());
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
	        	int indexSelect = institutionTable.getSelectionModel().getSelectedIndex();
		        mainApp.getListeInstitution().remove(indexSelect);
		    }    	
	    }
	    else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas d'institution selectionnée");
	        alert.setContentText("Veuillez selectionner une institution dans la table.");
	        alert.showAndWait();
	        }
	}

    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     * @param disable Vrai s'il ne faut pas rendre visible le bouton ok , faux sinon
     */
    public void setMainApp(MainApp mainApp,boolean disable) {
        this.mainApp = mainApp;
        
        if(disable) {
        	this.ok.setVisible(false);
        	this.annuler.setText("Fermer");
        }

        // Ajout de la liste de données observable dans la table
        institutionTable.setItems(mainApp.getListeInstitution());
    }

}
