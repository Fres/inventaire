/**
 * 
 */
package view;

import java.util.Optional;

import controller.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;

/**
 * Cette classe est utilisée pour gérer le menu contextuel
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class RootLayoutController {
	
	/**
	 * Element menu pour la gestion des utilisateurs
	 */
	@FXML
	MenuItem gestion_util;
	
	/**
	 * Element menu pour la gestion des materiels
	 */
	@FXML
	MenuItem gestion_mat;
	
	/**
	 * Element menu pour la gestion des stocks
	 */
	@FXML
	MenuItem gestion_stock;
	
	/**
	 * Element menu pour la gestion des institutions
	 */
	@FXML
	MenuItem gestion_insti;
	
	/**
	 * Element menu pour la gestion des lieux
	 */
	@FXML
	MenuItem gestion_lieu;
	
	/**
	 * Élément menu pour afficher l'aide
	 */
	@FXML
	MenuItem gestion_aide;
	
	/**
	 * Élément menu pour quitter l'application
	 */
	@FXML
	MenuItem gestion_quitter;

	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;
    
    
	public RootLayoutController() {
	}
	
	/**
	 * Méthode appelée lorsque l'utilisateur clique sur le menu "Gestion des utilisateurs..."
	 * Elle ouvre la fenêtre de gestion des utilisateurs
	 */
	@FXML
	private void gestionUtilisateur() {
	    boolean okClick = mainApp.interfaceAfficherListUtilisateur(true);
	    if (okClick) {}
	}
	
	/**
	 * Méthode appelée lorsque l'utilisateur clique sur le menu "Gestion des utilisateurs..."
	 * Elle ouvre la fenêtre de gestion des utilisateurs
	 */
	@FXML
	private void gestionMateriel() {
	    boolean okClick = mainApp.interfaceAfficherListeMateriel(true);
	    if (okClick) {}
	}
	
	/**
	 * Méthode appelée lorsque l'utilisateur clique sur le menu "Gestion des stocks..."
	 * Elle ouvre la fenêtre de gestion des stocks
	 */
	@FXML
	private void gestionStock() {
	    boolean okClick = mainApp.interfaceAfficherListeStock(true);
	    if (okClick) {}
	}
	
	/**
	 * Méthode appelée lorsque l'utilisateur clique sur le menu "Gestion des institutions..."
	 * Elle ouvre la fenêtre de gestion des institutionss
	 */
	@FXML
	private void gestionInstitution() {
	    boolean okClick = mainApp.interfaceAfficherListeInstitution(true);
	    if (okClick) {}
	}
	
	/**
	 * Méthode appelée lorsque l'utilisateur clique sur le menu "Gestion des lieu..."
	 * Elle ouvre la fenêtre de gestion des lieux
	 */
	@FXML
	private void gestionLieu() {
	    boolean okClick = mainApp.interfaceAfficherListeLieu(true);
	    if (okClick) {}
	}
	
	/**
	 * Méthode appelée lorsque l'utilisateur clique sur le menu "Quitter..."
	 */
	@FXML
	private void gestionQuitter() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Êtes-vous sûr de vouloir quitter l'application ?");
        Optional<ButtonType> option = alert.showAndWait();
       	    
        if (option.get() == ButtonType.OK) {
        	mainApp.save();
        	mainApp.getPrimaryStage().close();
        }
        else
            alert.close();
	}
	
	/**
     * Méthode appelée lorsque l'utilisateur clique sur About
     */
    @FXML
    private void gestionAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Inventaire");
        alert.setHeaderText("A Propos");
        alert.setContentText(" Application : Logiciel de gestion des emprunts\n Auteurs: Frescinel BART - Yoan BOITEUX");

        alert.showAndWait();
    }
    
    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;               
    }

}
