/**
 * 
 */
package view;

import model.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

import controller.MainApp;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;


/**
 * Cette classe est utilisée pour gérer les données dans le tableau des emprunts
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ListeEmpruntController {

	/**
	 * Table représentant la liste des emprunts
	*/
	@FXML
    private TableView<Emprunt> empruntTable;
	
	/**
	 * Colonne du tableau représentant la référence du matériel
	*/
    @FXML
    private TableColumn<Emprunt, String> ref;
    
	/**
	 * Colonne du tableau représentant le nom du matériel
	*/
    @FXML
    private TableColumn<Emprunt, String> nom_materiel;
    
	/**
	 * Colonne du tableau représentant le nom de l'emprunteur
	*/
    @FXML
    private TableColumn<Emprunt, String> nom_user;
    
	/**
	 * Colonne du tableau représentant la date de l'emprunt du matériel
	*/
    @FXML
    private TableColumn<Emprunt, Date> date_emprunt;
    
	/**
	 * Colonne du tableau représentant la date de retour du matériel
	*/
    @FXML
    private TableColumn<Emprunt,Date> date_retour;
    
    /**
     * Barre de recherche
     */
    @FXML
    private TextField recherche;
    
    /**
     * Liste des checkboxs
     */
    private ObservableList<CheckBox> selectedCheckBoxes = FXCollections.observableArrayList();

    /**
     * Liste des checkboxs pour les état des emprunts
     */
    private ObservableList<CheckBox> etatCheckBoxes = FXCollections.observableArrayList();
    
    /**
     * Checkbox etudiant
     */
    @FXML
    private CheckBox etudiant;
    
    /**
     * Checkbox enseignant
     */
    @FXML
    private CheckBox enseignant;
    
    /**
     * Checkbox startup
     */
    @FXML
    private CheckBox startup;
    
    /**
     * Checkbox en cours
     */
    @FXML
    private CheckBox cours;
    
    /**
     * Checkbox à rendre
     */
    @FXML
    private CheckBox rendre;
    
    /**
     * Checkbox en retard
     */
    @FXML
    private CheckBox retard;
    
    /**
     * Choice box représentant les types de matériel
     */
    @FXML
    private ChoiceBox<String> type_materiel;

	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;

    /**
     * Le constructeur du contrôleur ListeEmprunt
     * Le constructeur est appelé avant la méthode initialize
     */
    public ListeEmpruntController() {
    }

    /**
     * Méthode initialisant le contrôleur de la classe ListeEmpruntController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {

        // Initialise la table d'emprunt avec les cinq colonnes
        ref.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMat() !=null ? cellData.getValue().getMat().getRef() : null));
        nom_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMat() !=null ? cellData.getValue().getMat().getNom() : null));
        nom_user.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUtilisateur() !=null ? cellData.getValue().getUtilisateur().getNom() : null));
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        date_emprunt.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getDateEmprunt() !=null ? formatter.format(cellData.getValue().getDateEmprunt()) : null));
        date_retour.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getDateEmprunt() !=null ? formatter.format(cellData.getValue().getDateRetour()) : null));
        
        //Déclenche la recherche quand il y une saisie au clavier
        this.recherche.setOnKeyPressed(e -> {recherche(e);});
        
        //Ajout des checkbox dans la liste
        selectedCheckBoxes.addAll(this.etudiant,this.enseignant,this.startup);
        
        //Ajout des checkbox dans la liste des état d'un emprunt
        etatCheckBoxes.addAll(this.cours,this.rendre,this.retard);
        
        //Ajout des types de matériel dans la choicebox
        type_materiel.getItems().add("");
        type_materiel.getItems().add("Appareil");
        type_materiel.getItems().add("PeripheriqueEntree");
        type_materiel.getItems().add("PeripheriqueSortie");
        type_materiel.getItems().add("Souris");
        type_materiel.getItems().add("Telephone");
        type_materiel.getItems().add("Tablette");
        type_materiel.getItems().add("Capteur");
        type_materiel.getItems().add("ManetteJeu");
        type_materiel.getItems().add("Webcam");
        type_materiel.getItems().add("ManetteCasque");
        type_materiel.getItems().add("CasqueAudio");
        type_materiel.getItems().add("CasqueVR");
        
        //Déclenche le filtrage quand l'utilisateur choisi un type dans la choicebox
        this.type_materiel.setOnAction(e -> {filtreBox(e);});
                                       
        //Couleur appliquée aux lignes en fonction de la date de rendu du matériel
        empruntTable.setRowFactory(tv -> new TableRow<Emprunt>() {
            @Override
            protected void updateItem(Emprunt e, boolean empty) {
                super.updateItem(e, empty);
                
                if (e == null)
                    setStyle("");
                else if(e.getDateRetour()!=null) {
                    LocalDate dateRetour = e.getDateRetour().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    LocalDate today = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    if (today.isBefore(dateRetour))
                        setStyle("-fx-background-color: #baffba;");
                    else if (today.isAfter(dateRetour))
                    	setStyle("-fx-background-color: #FFB6C1;");
                    else if (today.equals(dateRetour))
                        setStyle("-fx-background-color: #FAFAD2;");
                    else
                        setStyle("");
                }
                
            }
        });
                
    }
    
    /**
     * Méthode permettant d'effectuer une recherche sur la table des emprunts
     * 
     *@param e Action déclenchée par la saisie du clavier
     */
	public void recherche(KeyEvent e)
	{
		FilteredList<Emprunt> filterData = new FilteredList<Emprunt>(mainApp.getListeEmprunt());
		this.recherche.textProperty().addListener((observableValue, olValue, newValue) ->{
			filterData.setPredicate((Predicate<? super Emprunt>) emprunt-> {
				if(newValue == null || newValue.isEmpty()){
					return true;
				}
				
				//Recherche selon le nom de l'emprunteur
				if((emprunt.getUtilisateur().getNom()).toLowerCase().contains(newValue.toLowerCase())){
					return true;
				}
				//Recherche selon le nom du matériel
				if(emprunt.getMat().getNom().toLowerCase().contains(newValue.toLowerCase())) {
					return true;
				}
				//Recherche selon la référence du matériel
				if(emprunt.getMat().getRef().toLowerCase().contains(newValue.toLowerCase())) {
					return true;
				}
				
				return false;
			});
		});
		
		SortedList<Emprunt> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.empruntTable.comparatorProperty());
		this.empruntTable.setItems(sortedData);
	}
	
    /**
     * Méthode permettant d'effectuer une recherche sur la table d'emprunts en fonction du type d'utilisateur ou/et du type de matériel
     * 
     *@param e Action déclenchée par la selection d'une checkbox ou d'une choicebox
     */
    public void filtreBox(ActionEvent e) {
		FilteredList<Emprunt> filterData = new FilteredList<Emprunt>(mainApp.getListeEmprunt());
		String newValue,Value="";
		String newEtat,Etat="";
		if(selectedCheckBoxes.size()!=0) {
			for (int i = 0; i < selectedCheckBoxes.size(); i++) {
				if(selectedCheckBoxes.get(i).isSelected())
					Value += " "+selectedCheckBoxes.get(i).textProperty().getValue();
			}
		}
		
		if(etatCheckBoxes.size()!=0) {
			for (int i = 0; i < etatCheckBoxes.size(); i++) {
				if(etatCheckBoxes.get(i).isSelected())
					Value += " "+etatCheckBoxes.get(i).textProperty().getValue();
			}
		}
		
		
		if("ChoiceBox".toLowerCase().indexOf(e.getSource().getClass().getSimpleName().toLowerCase()) != -1)
			Value +=(String) ((ChoiceBox<String>)e.getSource()).getSelectionModel().getSelectedItem();
	
		if(!Value.isEmpty()) newValue = Value;
		else newValue=null;
		
		
		filterData.setPredicate((Predicate<? super Emprunt>) emprunt-> {
			if(newValue == null || newValue.isEmpty())
					return true;
			
			//Recherche selon le type d'utilisateur
			if(newValue.toLowerCase().indexOf(emprunt.getUtilisateur().getClass().getSimpleName().toLowerCase()) != -1)
				return true;
			if(newValue.toLowerCase().indexOf(emprunt.getMat().getClass().getSimpleName().toLowerCase()) != -1)
				return true;	
			if(newValue.toLowerCase().indexOf(emprunt.getMat().getClass().getSuperclass().getSimpleName().toLowerCase()) != -1)
				return true;

			//Recherche selon l'état de l'emprunt
	        LocalDate dateRetour = emprunt.getDateRetour().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	        LocalDate today = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();	
	            
			if(newValue.toLowerCase().indexOf("En cours".toLowerCase()) != -1) {
	               if (today.isBefore(dateRetour))
	            	   return true;
			}
	        			
			if(newValue.toLowerCase().indexOf("À rendre".toLowerCase()) != -1) {
	              if (today.equals(dateRetour))
	               	return true;
			}
			if(newValue.toLowerCase().indexOf("En retard".toLowerCase()) != -1) {
	              if (today.isAfter(dateRetour))
	               	return true;
			}
			
				
			return false;
		});
		
		SortedList<Emprunt> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.empruntTable.comparatorProperty());
		this.empruntTable.setItems(sortedData);
    }
    
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter emprunt..."
	 * Elle ouvre une fenetre permettant d'ajouter un emprunt
	 */
	@FXML
	private void ajouterEmprunt() {
	    Emprunt emprunt = new Emprunt(null,null,null,null,null);
	    mainApp.getListeEmprunt().add(emprunt);
    	int index_last = mainApp.getListeEmprunt().size() - 1;
	    boolean okClick = mainApp.interfaceEditerEmprunt(mainApp.getListeEmprunt().get(index_last));
	    if (okClick)
	        mainApp.getListeEmprunt().set(index_last, mainApp.getListeEmprunt().get(index_last));
	    else
	    	mainApp.getListeEmprunt().remove(index_last);
	}

	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Modifier emprunt..."
	 * Elle ouvre une fenetre permettant de modifier l'emprunt selectionné dans la liste
	 */
	@FXML
	private void modifierEmprunt() {
	    Emprunt emprunt = empruntTable.getSelectionModel().getSelectedItem();
	    if (emprunt != null) {
	        boolean okClicked = mainApp.interfaceEditerEmprunt(emprunt);
	        if (okClicked) {
	        	int indexSelect = empruntTable.getSelectionModel().getSelectedIndex();
		        mainApp.getListeEmprunt().set(indexSelect, emprunt);
	        }

	    } else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas d'emprunt selectionné");
	        alert.setContentText("Veuillez selectionner un emprunt dans la table.");
	        alert.showAndWait();
	        }
	}
	
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Rendre emprunt..."
	 * Elle ouvre une fenetre de confirmation
	 */
	@FXML
	private void rendreEmprunt() {
        
	    Emprunt emprunt = empruntTable.getSelectionModel().getSelectedItem();
	    if(emprunt!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Rendre l'emprunt");
		    alert.setHeaderText("Confirmez-vous le rendu du matériel ?");
		    alert.setContentText(emprunt.getMat().toString());
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
	        	int indexSelect = empruntTable.getSelectionModel().getSelectedIndex();
	        	emprunt.getMat().retirerEmprunter(emprunt);
		        mainApp.getListeEmprunt().remove(indexSelect);
		    }    	
	    }
	    else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas d'emprunt selectionné");
	        alert.setContentText("Veuillez selectionner un emprunt dans la table.");
	        alert.showAndWait();
	        }
	}

    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Ajout de la liste de données observable dans la table
        empruntTable.setItems(mainApp.getListeEmprunt());
               
    }

}
