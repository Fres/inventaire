package view;

import java.util.ArrayList;

import controller.MainApp;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.*;

/**
 * Cette classe est utilisée pour ajouter ou modifier stock dans la fenetre AjoutInstitution
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class AjoutStockController {
	
	/**
	 * Champs représentant le numero de stock
	 */
    @FXML
    private Label num_stock;
    
	/**
	 * Champs représentant le nom du lieu où est mis le stock
	 */
    @FXML
    private Label lieu;
    
    /**
	 * Table représentant la liste des matériels
	*/
	@FXML
    private TableView<Materiel> materielTable;
	
	/**
	 * Colonne du tableau représentant la référence du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> ref;
    
	/**
	 * Colonne du tableau représentant le nom du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> nom_materiel;
    
	/**
	 * Colonne du tableau représentant le marque du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> marque_materiel;
    
	/**
	 * Colonne du tableau représentant l'état du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> etat_materiel;
    
        
    private Stage dialogStage;
    private Stock stock;
    private boolean okClick = false;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;

    /**
     * Méthode initialisant le contrôleur de la classe AjoutStockController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {
    	
        // Initialise la table de matériel
        ref.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRef()));
        nom_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        marque_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMarque()));
        etat_materiel.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getEtat()));
        
        //Selection multiple de matériels
        materielTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'afficher les détails d'un stock
     *
     * @param stock Le stock
     */
    public void setStock(Stock stock) {
 
        this.stock = stock;

        num_stock.setText(Integer.toString(stock.getId_stock()));
        
    	if(stock.getLieu()!=null) {
    	    lieu.setText(stock.getLieu().getNom_lieu());
    	}
    	
    	if(stock.getListe_m().size()!=0) {
    		for(int i=0;i<stock.getListe_m().size();i++) {
    			materielTable.getSelectionModel().select(stock.getListe_m().get(i));
    		}
    	}
  
    }

    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }

    /**
     * Méthode appelé lorsque l'utilisateur clique sur OK pour valider le stock
     */
    @FXML
    private void gestionOk() {
        if (isInputValid()) {       	
        	stock.setListe_m(new ArrayList<Materiel>());
        	stock.ajouterMateriel(materielTable.getSelectionModel().getSelectedItems());
        	
    		okClick = true;
            dialogStage.close();
        }
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un stock
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
    
    /**
     * Méthode permettant d'effacer les saisies
     */
    @FXML
    private void gestionEffacer() {

    }
    
    /**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Choisir un lieu..."
	 * Elle ouvre une fenetre permettant d'ajouter un lieu
     */
    @FXML
	private void ajouterLieu() {
	    boolean okClick = mainApp.interfaceAfficherListeLieu(false);
	    if (okClick) {
	    	int index_last = mainApp.getListeStock().size() - 1;
	    	stock = mainApp.getListeStock().get(index_last);
	        this.setStock(stock);
	    }
	}
    
    
    /**
     * Méthode permettant de gérer les saisies au clavier de l'utilisateurs
     *
     * @return Vrai si l'utilisation a bien saisie les champs, faux sinon
     */
    private boolean isInputValid() {
	    String errorMessage = "";
	
	    if (lieu.getText()==null || lieu.getText().isEmpty()) {
	        errorMessage += "Veuillez rentrer un lieu !\n";
	    }
        if(materielTable.getSelectionModel().getSelectedItem()==null) {
            errorMessage += "Veuillez selectionner le matériel qui doit être stocké\n";
        }

        if (errorMessage.length() == 0) return true;
        else {
            //Afficher un message d'erreur
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalides");
            alert.setHeaderText("Veuillez corriger les champs invalides");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
    
    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;  
        
        // Ajout des matériels dans la table view des matériels
    	for (int i = 0; i <this.mainApp.getListeMateriel().size(); i++) {
    		materielTable.getItems().add(this.mainApp.getListeMateriel().get(i));
		}
    }
}
