/**
 * 
 */
package view;

import model.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

import controller.MainApp;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;


/**
 * Cette classe est utilisée pour gérer les données dans le tableau des lieux
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ListeLieuController {

	/**
	 * Table représentant la liste des lieux
	*/
	@FXML
    private TableView<Lieu> lieuTable;
	
	/**
	 * Colonne du tableau représentant le nom du lieu
	*/
    @FXML
    private TableColumn<Lieu, String> nom_lieu;
    
	/**
	 * Colonne du tableau représentant le nom de l'institution
	*/
    @FXML
    private TableColumn<Lieu, String> nom_insti;
    
	/**
	 * Colonne du tableau représentant le nombre de stocks
	*/
    @FXML
    private TableColumn<Lieu, Number> nombre_stocks;
    
    /**
     * Barre de recherche
     */
    @FXML
    private TextField recherche;
    
    /**
     * Liste des checkboxs
     */
    private ObservableList<CheckBox> selectedCheckBoxes = FXCollections.observableArrayList();
    
    /**
     * Bouton ok
     */
    @FXML
    private Button ok;
    
    /**
     * Bouton annuler
     */
    @FXML
    private Button annuler;

	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;
    private Stage dialogStage;
    private boolean okClick = false;

    /**
     * Le constructeur du contrôleur ListeLieu
     * Le constructeur est appelé avant la méthode initialize
     */
    public ListeLieuController() {
    }

    /**
     * Méthode initialisant le contrôleur de la classe ListeLieuController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {

        // Initialise la table d'emprunt avec les cinq colonnes
        nom_lieu.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom_lieu() !=null ? cellData.getValue().getNom_lieu() : null));
        nom_insti.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getInsti() !=null ? cellData.getValue().getInsti().getName() : null));
        nombre_stocks.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getListe_s().size()));

        //Déclenche la recherche quand il y une saisie au clavier
        this.recherche.setOnKeyPressed(e -> {recherche(e);});    
                
    }
    
    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    /**
     * Méthode permettant d'effectuer une recherche sur la table des lieux
     * 
     *@param e Action déclenchée par la saisie du clavier
     */
	public void recherche(KeyEvent e)
	{
		FilteredList<Lieu> filterData = new FilteredList<Lieu>(mainApp.getListeLieu());
		this.recherche.textProperty().addListener((observableValue, olValue, newValue) ->{
			filterData.setPredicate((Predicate<? super Lieu>) lieu-> {
				if(newValue == null || newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter = newValue.toLowerCase();
				
				//Recherche selon le nom de l'institution
				if(lieu.getInsti().getName().toLowerCase().contains(newValue.toLowerCase())){
					return true;
				}
				//Recherche selon le nom du lieu
				if(lieu.getNom_lieu().toLowerCase().contains(newValue.toLowerCase())) {
					return true;
				}
				
				return false;
			});
		});
		
		SortedList<Lieu> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.lieuTable.comparatorProperty());
		this.lieuTable.setItems(sortedData);
	}
	
    /**
     * Méthode permettant d'effectuer une recherche sur la table des lieux en fonction du type d'utilisateur 
     * 
     *@param e Action déclenchée par la selection d'une checkbox ou d'une choicebox
     */
    public void filtreBox(ActionEvent e) {
		FilteredList<Lieu> filterData = new FilteredList<Lieu>(mainApp.getListeLieu());
		String newValue,Value="";
		String newEtat,Etat="";
		if(selectedCheckBoxes.size()!=0) {
			for (int i = 0; i < selectedCheckBoxes.size(); i++) {
				if(selectedCheckBoxes.get(i).isSelected())
					Value += " "+selectedCheckBoxes.get(i).textProperty().getValue();
			}
		}	
	
		if(!Value.isEmpty()) newValue = Value;
		else newValue=null;
		
		
		filterData.setPredicate((Predicate<? super Lieu>) lieu-> {
			if(newValue == null || newValue.isEmpty())
					return true;
			
			//Recherche selon le type d'utilisateur
			if(newValue.toLowerCase().indexOf(lieu.getRespo().getClass().getSimpleName().toLowerCase()) != -1)
				return true;
			if(newValue.toLowerCase().indexOf(lieu.getInsti().getClass().getSimpleName().toLowerCase()) != -1)
				return true;	
		
				
			return false;
		});
		
		SortedList<Lieu> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.lieuTable.comparatorProperty());
		this.lieuTable.setItems(sortedData);
    }
    
    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }
    
    /**
     * Méthode appelée lorsque l'utilisateur clique sur OK pour valider le stock
     */
    @FXML
    private void gestionOk() {

    	int index_last = mainApp.getListeStock().size() - 1;
        Lieu lieu = lieuTable.getSelectionModel().getSelectedItem();
        mainApp.getListeStock().get(index_last).setLieu(lieu);
        okClick = true;
        dialogStage.close();
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un materiel
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
    
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter lieu..."
	 * Elle ouvre une fenetre permettant d'ajouter un lieu
	 */
	@FXML
	private void ajouterLieu() {
	    Lieu lieu = new Lieu(null,null,null);
	    mainApp.getListeLieu().add(lieu);
	    int index_last = mainApp.getListeLieu().size() - 1;
	    boolean okClick = mainApp.interfaceGestionLieu(mainApp.getListeLieu().get(index_last));
	    if (okClick)
	        mainApp.getListeLieu().set(index_last, mainApp.getListeLieu().get(index_last));
	    else
	    	mainApp.getListeLieu().remove(index_last);
	}

	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Modifier lieu..."
	 * Elle ouvre une fenetre permettant de modifier le lieu selectionné dans la liste
	 */
	@FXML
	private void modifierLieu() {
	    Lieu lieu = lieuTable.getSelectionModel().getSelectedItem();
	    if (lieu != null) {
	        boolean okClicked = mainApp.interfaceGestionLieu(lieu);
	        if (okClicked) {
	        	int indexSelect = lieuTable.getSelectionModel().getSelectedIndex();
		        mainApp.getListeLieu().set(indexSelect, lieu);
	        }

	    } else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas de lieu selectionné");
	        alert.setContentText("Veuillez selectionner un lieu dans la table.");
	        alert.showAndWait();
	        }
	}
	
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Supprimer un lieu..."
	 * Elle ouvre une fenetre de confirmation
	 */
	@FXML
	private void supprimerLieu() {
        
	    Lieu lieu = lieuTable.getSelectionModel().getSelectedItem();
	    if(lieu!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Supprimer le lieu");
		    alert.setHeaderText("Confirmez-vous la suppression du lieu?");
		    alert.setContentText(lieu.getNom_lieu());
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
	        	int indexSelect = lieuTable.getSelectionModel().getSelectedIndex();
		        mainApp.getListeLieu().remove(indexSelect);
		    }    	
	    }
	    else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas de lieu selectionné");
	        alert.setContentText("Veuillez selectionner un lieu dans la table.");
	        alert.showAndWait();
	        }
	}

    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     * @param disable Vrai s'il ne faut pas rendre visible le bouton ok , faux sinon
     */
    public void setMainApp(MainApp mainApp,boolean disable) {
        this.mainApp = mainApp;
        
        if(disable) {
        	this.ok.setVisible(false);
        	this.annuler.setText("Fermer");
        }

        // Ajout de la liste de données observable dans la table
        lieuTable.setItems(mainApp.getListeLieu());
    }

}
