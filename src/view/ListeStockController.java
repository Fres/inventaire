/**
 * 
 */
package view;

import java.util.Optional;
import java.util.function.Predicate;

import model.*;
import controller.MainApp;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Cette classe est utilisée pour gérer les données dans le tableau des stocks
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ListeStockController {
	/**
	 * Table représentant la liste des stocks
	*/
	@FXML
    private TableView<Stock> stockTable;
	
	/**
	 * Colonne du tableau représentant l'id du stock
	*/
    @FXML 
    private TableColumn<Stock, Number> id_stock;
    
	/**
	 * Colonne du tableau représentant le nom du lieu
	*/
    @FXML
    private TableColumn<Stock, String> lieu_stock;
    
	/**
	 * Colonne du tableau représentant le nombre de matériel
	*/
    @FXML
    private TableColumn<Stock, Number> nombre_mat_stock;
        
    /**
     * Bouton ok
     */
    @FXML
    private Button ok;
    
    /**
     * Bouton annuler
     */
    @FXML
    private Button annuler;
    
    /**
     * Barre de recherche
     */
    @FXML
    private TextField recherche;
    
    /**
     * Choice box représentant les types de matériel
     */
    @FXML
    private ChoiceBox<String> type_materiel;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;
    
    private Stage dialogStage;
    private boolean okClick = false;

    /**
     * Le constructeur du contrôleur ListeStock
     * Le constructeur est appelé avant la méthode initialize
     */
    public ListeStockController() {
    }

    /**
     * Méthode initialisant le contrôleur de la classe ListeStockController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {

        // Initialise la table des utilisateurs
    	
        id_stock.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getId_stock()));
        lieu_stock.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLieu()!=null ? cellData.getValue().getLieu().getNom_lieu():null));
        nombre_mat_stock.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getListe_m().size()));
                
        //Déclenche la recherche quand il y a une saisie au clavier
        this.recherche.setOnKeyPressed(e -> {recherche(e);});
        
        //Ajout des types de matériel dans la choicebox
        type_materiel.getItems().add("");
        type_materiel.getItems().add("Appareil");
        type_materiel.getItems().add("PeripheriqueEntree");
        type_materiel.getItems().add("PeripheriqueSortie");
        type_materiel.getItems().add("Souris");
        type_materiel.getItems().add("Telephone");
        type_materiel.getItems().add("Tablette");
        type_materiel.getItems().add("Capteur");
        type_materiel.getItems().add("ManetteJeu");
        type_materiel.getItems().add("Webcam");
        type_materiel.getItems().add("ManetteCasque");
        type_materiel.getItems().add("CasqueAudio");
        type_materiel.getItems().add("CasqueVR");
        
        this.type_materiel.setOnAction(e -> {filtreBox(e);});
        
    }
    
    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'effectuer une recherche sur la table des stocks selon le type de matériel
     * 
     *@param e Action déclenchée par la selection d'une choicebox
     */
    public void filtreBox(ActionEvent e) {
		FilteredList<Stock> filterData = new FilteredList<Stock>(mainApp.getListeStock());
		String newValue,Value="";
		
		if("ChoiceBox".toLowerCase().indexOf(e.getSource().getClass().getSimpleName().toLowerCase()) != -1)
			Value +=(String) ((ChoiceBox<String>)e.getSource()).getSelectionModel().getSelectedItem();
	
		if(!Value.isEmpty()) newValue = Value;
		else newValue=null;
		
		filterData.setPredicate((Predicate<? super Stock>) stock-> {
			if(newValue == null || newValue.isEmpty())
					return true;
			
			//Recherche selon le type de matériel
			int i=0;
			boolean trouve=false;
			while(!trouve && i<stock.getListe_m().size()) {
				if(newValue.toLowerCase().indexOf(stock.getListe_m().get(i).getClass().getSimpleName().toLowerCase()) != -1) {
					trouve=true;
					return true;
				}
				else if(newValue.toLowerCase().indexOf(stock.getListe_m().get(i).getClass().getSuperclass().getSimpleName().toLowerCase()) != -1) {
					trouve=true;
					return true;
				}
					
				i++;
			}
				
			return false;
		});
		
		SortedList<Stock> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.stockTable.comparatorProperty());
		this.stockTable.setItems(sortedData);
    }
    
    /**
     * Méthode permettant d'effectuer une recherche sur la table des matériels
     * 
     *@param e Action déclenchée par la saisie du clavier
     */
	public void recherche(KeyEvent e)
	{
		FilteredList<Stock> filterData = new FilteredList<Stock>(mainApp.getListeStock());
		this.recherche.textProperty().addListener((observableValue, olValue, newValue) ->{
			filterData.setPredicate((Predicate<? super Stock>) stock-> {
				if(newValue == null || newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter = newValue.toLowerCase();
				
				//Recherche selon la référence du matériel
				int i=0;
				boolean trouve=false;
				while(!trouve && i<stock.getListe_m().size()) {
					if(stock.getListe_m().get(i).getRef().toLowerCase().contains(newValue.toLowerCase())){
						trouve=true;
						return true;
					}
					//Recherche selon la marque du matériel
					if(stock.getListe_m().get(i).getMarque().toLowerCase().contains(newValue.toLowerCase())){
						trouve=true;
						return true;
					}
					//Recherche selon l'état du matériel
					if(stock.getListe_m().get(i).getEtat().equals(newValue)){
						trouve=true;
						return true;
					}
					i++;
				}
				
				//Recherche selon le nom du lieu du stock
				if(stock.getLieu().getNom_lieu().contains(newValue)){
					return true;
				}
					
				
				return false;
			});
		});
		
		SortedList<Stock> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.stockTable.comparatorProperty());
		this.stockTable.setItems(sortedData);
	}
	
    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }
    
    /**
     * Méthode appelée lorsque l'utilisateur clique sur OK pour valider le stock
     */
    @FXML
    private void gestionOk() {

    	/*int index_last = mainApp.getListeEmprunt().size() - 1;
        Utilisateur utilisateur = utilisateurTable.getSelectionModel().getSelectedItem();
        mainApp.getListeEmprunt().get(index_last).setUtilisateur(utilisateur);*/
        okClick = true;
        dialogStage.close();
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un materiel
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
	
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..."
	 * Elle ouvre une fenetre permettant d'ajouter un stock
	 */
	@FXML
	private void ajouterStock() {
	    Stock stock = new Stock(null);
	    mainApp.getListeStock().add(stock);
    	int index_last = mainApp.getListeStock().size() - 1;
	    boolean okClick = mainApp.interfaceGestionStock(mainApp.getListeStock().get(index_last));
	    if (okClick)
	        mainApp.getListeStock().set(index_last, mainApp.getListeStock().get(index_last));
	    else
	    	mainApp.getListeStock().remove(index_last);
	}

	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Modifier..."
	 * Elle ouvre une fenetre permettant de modifier un stock
	 */
	@FXML
	private void modifierStock() {
		Stock stock = stockTable.getSelectionModel().getSelectedItem();
	    if (stock != null) {
	        boolean okClicked = mainApp.interfaceGestionStock(stock);
	        if (okClicked) {
	        	int indexSelect = stockTable.getSelectionModel().getSelectedIndex();
		        mainApp.getListeStock().set(indexSelect, stock);
	        }

	    } else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas de stock selectionné");
	        alert.setContentText("Veuillez selectionner un stock dans la table.");
	        alert.showAndWait();
	        }
	}
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Supprimer..."
	 * Elle ouvre une fenetre de confirmation
	 */
	@FXML
	private void supprimerStock() {
        
		Stock stock = stockTable.getSelectionModel().getSelectedItem();
	    if(stock!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Supprimer un stock");
		    alert.setHeaderText("Confirmez-vous la suppression du stock?");
		    alert.setContentText("Stock n° "+stock.getId_stock()+" stocké dans "+stock.getLieu().getNom_lieu());
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
	        	int indexSelect = stockTable.getSelectionModel().getSelectedIndex();
	        	if(stock.getListe_m().size()!=0) {
	        		for(int i=0;i<stock.getListe_m().size();i++) {
	        			stock.supprimerMateriel(stock.getListe_m().get(i));
	        		}
	        	}
		        mainApp.getListeStock().remove(indexSelect);
		    }    	
	    }
	    else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas de stock selectionné");
	        alert.setContentText("Veuillez selectionner un stock dans la table.");
	        alert.showAndWait();
	        }
	}

    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     * @param disable Vrai s'il ne faut pas rendre visible le bouton ok , faux sinon
     */
    public void setMainApp(MainApp mainApp,boolean disable) {
        this.mainApp = mainApp;
        
        if(disable) {
        	this.ok.setVisible(false);
        	this.annuler.setText("Fermer");
        }

        // Ajout de la liste de données observable dans la table
        stockTable.setItems(mainApp.getListeStock());
    }

}
