/**
 * 
 */
package view;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

import model.*;
import model.Materiel.Etat;
import controller.MainApp;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Cette classe est utilisée pour gérer les données dans le tableau des matériels
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ListeMaterielController {
	/**
	 * Table représentant la liste matériels
	*/
	@FXML
    private TableView<Materiel> materielTable;
	
	/**
	 * Colonne du tableau représentant la référence du matériel
	*/
    @FXML 
    private TableColumn<Materiel, String> ref_materiel;
    
	/**
	 * Colonne du tableau représentant le nom du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> nom_materiel;
    
	/**
	 * Colonne du tableau représentant la marque du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> marque_materiel;
    
	/**
	 * Colonne du tableau représentant le prix du matériel
	*/
    @FXML
    private TableColumn<Materiel, Double> prix_materiel;
    
	/**
	 * Colonne du tableau représentant l'etat du matériel
	*/
    @FXML
    private TableColumn<Materiel, Etat> etat_materiel;
    
	/**
	 * Colonne du tableau représentant la date d'achat du matériel
	*/
    @FXML
    private TableColumn<Materiel, Date> achat_materiel;
    
	/**
	 * Colonne du tableau représentant la disponibilité du matériel
	*/
    @FXML
    private TableColumn<Materiel, String> dispo_materiel;
    
    
    /**
     * Barre de recherche
     */
    @FXML
    private TextField recherche;
    
    /**
     * Choice box représentant les types de matériel
     */
    @FXML
    private ChoiceBox<String> type_materiel;
    
    /**
     * Bouton ok
     */
    @FXML
    private Button ok;
    
    /**
     * Bouton annuler
     */
    @FXML
    private Button annuler;
    
    /**
     * Liste des checkboxs
     */
    private ObservableList<CheckBox> selectedCheckBoxes = FXCollections.observableArrayList();
    
    /**
     * Checkbox disponible
     */
    @FXML
    private CheckBox dispo;
    
    /**
     * Checkbox non disponible
     */
    @FXML
    private CheckBox non_dispo;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;
    
    private Stage dialogStage;
    private boolean okClick = false;

    /**
     * Le constructeur du contrôleur ListeMateriel
     * Le constructeur est appelé avant la méthode initialize
     */
    public ListeMaterielController() {
    }

    /**
     * Méthode initialisant le contrôleur de la classe ListeMaterielController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {

        // Initialise la table des utilisateurs
    	
        ref_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRef()));
        nom_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        marque_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMarque()));
        prix_materiel.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getPrix()));
        etat_materiel.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getEtat()));
        achat_materiel.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getDateAchat()));
        dispo_materiel.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().estDispo() ? "Oui" : "Non"));

        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        achat_materiel.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getDateAchat() !=null ? formatter.format(cellData.getValue().getDateAchat()) : null));        
        
        
        //Déclenche la recherche quand il y a une saisie au clavier
        this.recherche.setOnKeyPressed(e -> {recherche(e);});
        
        //Ajout des checkbox dans la liste
        selectedCheckBoxes.addAll(this.dispo,this.non_dispo);

        //Déclenche la recherche quand il y a une checkbox a été selectionné
        this.dispo.setOnAction(e -> {filtreBox(e);});
        this.non_dispo.setOnAction(e -> {filtreBox(e);});
        
        //Ajout des types de matériel dans la choicebox
        type_materiel.getItems().add("");
        type_materiel.getItems().add("Appareil");
        type_materiel.getItems().add("PeripheriqueEntree");
        type_materiel.getItems().add("PeripheriqueSortie");
        type_materiel.getItems().add("Souris");
        type_materiel.getItems().add("Telephone");
        type_materiel.getItems().add("Tablette");
        type_materiel.getItems().add("Capteur");
        type_materiel.getItems().add("ManetteJeu");
        type_materiel.getItems().add("Webcam");
        type_materiel.getItems().add("ManetteCasque");
        type_materiel.getItems().add("CasqueAudio");
        type_materiel.getItems().add("CasqueVR");
        
        this.type_materiel.setOnAction(e -> {filtreBox(e);});
        
    }
    
    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'effectuer une recherche sur la table des matériels
     *  
     *@param e Action déclenchée par la selection d'une checkbox ou d'une choicebox
     */
    public void filtreBox(ActionEvent e) {
		FilteredList<Materiel> filterData = new FilteredList<Materiel>(mainApp.getListeMateriel());
		String newValue,Value="";
		String newEtat,Etat="";
		if(selectedCheckBoxes.size()!=0) {
			for (int i = 0; i < selectedCheckBoxes.size(); i++) {
				if(selectedCheckBoxes.get(i).isSelected())
					Value += " "+selectedCheckBoxes.get(i).textProperty().getValue();
			}
		}		
		
		if("ChoiceBox".toLowerCase().indexOf(e.getSource().getClass().getSimpleName().toLowerCase()) != -1)
			Value +=(String) ((ChoiceBox<String>)e.getSource()).getSelectionModel().getSelectedItem();
	
		if(!Value.isEmpty()) newValue = Value;
		else newValue=null;
		
		
		filterData.setPredicate((Predicate<? super Materiel>) materiel-> {
			if(newValue == null || newValue.isEmpty())
					return true;
			
			if(newValue.contains("Disponible")) {
				if(materiel.estDispo())
					return true;
			}
			
			if(newValue.contains("Non dispo")) {
				if(!materiel.estDispo())
					return true;
			}
			
			if(newValue.toLowerCase().indexOf(materiel.getClass().getSimpleName().toLowerCase()) != -1)
				return true;
			if(newValue.toLowerCase().indexOf(materiel.getClass().getSuperclass().getSimpleName().toLowerCase()) != -1)
				return true;	
			
				
			return false;
		});
		
		SortedList<Materiel> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.materielTable.comparatorProperty());
		this.materielTable.setItems(sortedData);
    }
    
    /**
     * Méthode permettant d'effectuer une recherche sur la table des matériels
     * 
     *@param e Action déclenchée par la saisie du clavier
     */
	public void recherche(KeyEvent e)
	{
		FilteredList<Materiel> filterData = new FilteredList<Materiel>(mainApp.getListeMateriel());
		this.recherche.textProperty().addListener((observableValue, olValue, newValue) ->{
			filterData.setPredicate((Predicate<? super Materiel>) materiel-> {
				if(newValue == null || newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter = newValue.toLowerCase();
				
				//Recherche selon la référence du matériel
				if(materiel.getRef().toLowerCase().contains(newValue.toLowerCase())){
					return true;
				}
				//Recherche selon le nom du matériel
				if(materiel.getNom().toLowerCase().contains(newValue.toLowerCase())){
					return true;
				}
				//Recherche selon la marque du matériel
				if(materiel.getMarque().toLowerCase().contains(newValue.toLowerCase())){
					return true;
				}
				//Recherche selon l'état du matériel
				if(materiel.getEtat().toString().toLowerCase().contains(newValue.toLowerCase())){
					return true;
				}
		
				
				return false;
			});
		});
		
		SortedList<Materiel> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.materielTable.comparatorProperty());
		this.materielTable.setItems(sortedData);
	}
	
    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }
    
    /**
     * Méthode appelée lorsque l'utilisateur clique sur OK pour valider le materiel
     */
    @FXML
    private void gestionOk() {

    	/*int index_last = mainApp.getListeEmprunt().size() - 1;
        Utilisateur utilisateur = utilisateurTable.getSelectionModel().getSelectedItem();
        mainApp.getListeEmprunt().get(index_last).setUtilisateur(utilisateur);*/
        okClick = true;
        dialogStage.close();
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un materiel
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
	
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..." puis "Appareil
	 * Elle ouvre une fenetre permettant d'ajouter un appareil
	 */
	@FXML
	private void ajouterAppareil() {
	    Appareil app = new Appareil(null,null,null,0.0,null,null,0.0,null);
	    mainApp.getListeMateriel().add(app);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick) {
		    System.out.println(mainApp.getListeMateriel().get(index_last));
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    }
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	@FXML
	private void ajouterTelephone() {
	    Telephone app = new Telephone(null,null,null,0.0,null,null,0.0,null);
	    mainApp.getListeMateriel().add(app);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick) {
		    System.out.println(mainApp.getListeMateriel().get(index_last));
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    }
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	@FXML
	private void ajouterTablette() {
	    Tablette app = new Tablette(null,null,null,0.0,null,null,0.0,null);
	    mainApp.getListeMateriel().add(app);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick) {
		    System.out.println(mainApp.getListeMateriel().get(index_last));
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    }
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..." puis "Peripherique Entree
	 * Elle ouvre une fenetre permettant d'ajouter un Peripherique Entree
	 */
	@FXML
	private void ajouterPeripheriqueEntree() {
		PeripheriqueEntree entree = new PeripheriqueEntree(null,null,null,0.0,null,0,null);
	    mainApp.getListeMateriel().add(entree);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	@FXML
	private void ajouterSouris() {
		Souris entree= new Souris(null,null,null,0.0,null,0,null);
	    mainApp.getListeMateriel().add(entree);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	@FXML
	private void ajouterCapteur() {
		Capteur entree= new Capteur(null,null,null,0.0,null,0,null);
	    mainApp.getListeMateriel().add(entree);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	@FXML
	private void ajouterJeu() {
		ManetteJeu entree= new ManetteJeu(null,null,null,0.0,null,0,null);
	    mainApp.getListeMateriel().add(entree);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	@FXML
	private void ajouterWebcam() {
		Webcam entree= new Webcam(null,null,null,0.0,null,0,null);
	    mainApp.getListeMateriel().add(entree);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	@FXML
	private void ajouterManetteCasque() {
		ManetteCasque entree= new ManetteCasque(null,null,null,0.0,null,0,null,null);
	    mainApp.getListeMateriel().add(entree);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..." puis "Peripherique Sortie
	 * Elle ouvre une fenetre permettant d'ajouter un Peripherique Sortie
	 */
	@FXML
	private void ajouterPeripheriqueSortie() {
		PeripheriqueSortie sortie = new PeripheriqueSortie(null,null,null,0.0,null,null);
	    mainApp.getListeMateriel().add(sortie);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	
	@FXML
	private void ajouterCasqueAudio() {
		CasqueAudio sortie = new CasqueAudio(null,null,null,0.0,null,null);
	    mainApp.getListeMateriel().add(sortie);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}
	@FXML
	private void ajouterCasqueVR() {
		CasqueVR sortie = new CasqueVR(null,null,null,0.0,null,null);
	    mainApp.getListeMateriel().add(sortie);
    	int index_last = mainApp.getListeMateriel().size() - 1;
	    boolean okClick = mainApp.interfaceGestionMateriel(mainApp.getListeMateriel().get(index_last));
	    if (okClick)
	        mainApp.getListeMateriel().set(index_last, mainApp.getListeMateriel().get(index_last));
	    else
	    	mainApp.getListeMateriel().remove(index_last);
	}

	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Modifier..."
	 * Elle ouvre une fenetre permettant de modifier un matériel
	 */
	@FXML
	private void modifierMateriel() {
	    Materiel materiel = materielTable.getSelectionModel().getSelectedItem();
	    if (materiel != null) {
	        boolean okClicked = mainApp.interfaceGestionMateriel(materiel);
	        if (okClicked) {
	        	int indexSelect = materielTable.getSelectionModel().getSelectedIndex();
	        	materielTable.getItems().set(indexSelect, materiel);
	        }

	    } else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas de matériel selectionné");
	        alert.setContentText("Veuillez selectionner un matériel dans la table.");
	        alert.showAndWait();
	        }
	}
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Supprimer..."
	 * Elle ouvre une fenetre de confirmation
	 */
	@FXML
	private void supprimerMateriel() {
        
	    Materiel materiel= materielTable.getSelectionModel().getSelectedItem();
	    if(materiel!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Supprimer le materiel");
		    alert.setHeaderText("Confirmez-vous la suppression du matériel?");
		    alert.setContentText(materiel.getNom()+" "+materiel.getRef());
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
	        	int indexSelect = materielTable.getSelectionModel().getSelectedIndex();
	        	this.supprimerEmprunts(materiel);
		        mainApp.getListeMateriel().remove(indexSelect);
		    }    	
	    }
	    else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas d'utilisateur selectionné");
	        alert.setContentText("Veuillez selectionner un utilisateur dans la table.");
	        alert.showAndWait();
	        }
	}
	
	/**
	 * Méthode permettant de supprimer les emprunts ayant le matériel en paramètre
	 * 
	 * @param mat Le matériel dont l'emprunt va être supprimé
	 */
	public void supprimerEmprunts(Materiel mat) {
		int index=0;
    	while(index<mainApp.getListeEmprunt().size()) {
    		if(mat.equals(mainApp.getListeEmprunt().get(index).getMat())) {
    			mainApp.getListeEmprunt().get(index).getMat().retirerEmprunter(mainApp.getListeEmprunt().get(index));
				mainApp.getListeEmprunt().remove(index);
    		}
    		index++;
    	}
	}

    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     * @param disable Vrai s'il ne faut pas rendre visible le bouton ok , faux sinon
     */
    public void setMainApp(MainApp mainApp,boolean disable) {
        this.mainApp = mainApp;
        
        if(disable) {
        	this.ok.setVisible(false);
        	this.annuler.setText("Fermer");
        }

        // Ajout de la liste de données observable dans la table
        
        materielTable.setItems(mainApp.getListeMateriel());
    }

}
