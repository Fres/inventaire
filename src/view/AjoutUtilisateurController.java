package view;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import controller.MainApp;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;

/**
 * Cette classe est utilisée pour ajouter ou modifier un utilisateur dans la fenetre AjoutUtilisateur
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class AjoutUtilisateurController {

	/**
	 * Champs représentant le nom de l'utilisateur
	 */
    @FXML
    private TextField nom_util;
    
	/**
	 * Champs représentant le prénom de l'utilisateur
	 */
    @FXML
    private TextField prenom_util;

    /**
     * Champs représentant l'adresse de l'utilisateur
     */
    @FXML
    private TextField adresse_util;

    /**
     * Champs représentant le numéro de téléphone de l'utilisateur
     */
    @FXML
    private TextField tel_util;
    
    /**
     * Champs représentant l'adresse email de l'utilisateur
     */
    @FXML
    private TextField mail_util;
        
    /**
     * Champs représentant le nom de la formation de l'étudiant
     */
    @FXML
    private TextField formation_etu;
    
    /**
     * Champs représentant le nom de la formation de l'étudiant
     */
    @FXML
    private Label label_formation_etu;
    
    /**
     * Champs représentant l'année de formation de l'étudiant
     */
    @FXML
    private TextField annee_etu;
    
    /**
     * Champs représentant l'année de formation de l'étudiant
     */
    @FXML
    private Label label_annee_etu;
    
    /**
     * Champs représentant la matière enseignée par l'enseignant
     */
    @FXML
    private TextField enseign_ens;
    
    /**
     * Champs représentant la matière enseignée par l'enseignant
     */
    @FXML
    private Label label_enseign_ens;
    
    /**
     * Champs représentant la fonction admin de l'enseignant ou non
     */
    @FXML
    private CheckBox admin_ens;
    
    /**
     * Champs représentant la fonction admin de l'enseignant ou non
     */
    @FXML
    private Label label_admin_ens;
    
    
    
    /**
     * Champs représentant la raison social de la startUp
     */
    @FXML
    private TextField raison_start;
    
    /**
     * Champs représentant la raison social de la startUp
     */
    @FXML
    private Label label_raison_start;
    
    
    private Stage dialogStage;
    private Utilisateur utilisateur;
    private boolean okClick = false;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;

    /**
     * Méthode initialisant le contrôleur de la classe AjoutUtilisateurController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {
    
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'afficher les détails d'un utilisateur
     *
     * @param utilisateur L'utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur){
 
    	this.utilisateur = utilisateur;

        if(utilisateur.getNom()!=null)
            nom_util.setText(utilisateur.getNom());
        if(utilisateur.getPrenom()!=null)
            prenom_util.setText(utilisateur.getPrenom());
        if(utilisateur.getAdresse()!=null)
            adresse_util.setText(utilisateur.getAdresse());
        if(utilisateur.getNum_tel()!=null)
            tel_util.setText(utilisateur.getNum_tel());
        if(utilisateur.getMail()!=null)
            mail_util.setText(utilisateur.getMail());
                
        if(utilisateur.getClass().getSimpleName().equals("Etudiant")) {
        	this.annee_etu.setText(((Etudiant) utilisateur).getAnnee());
        	this.formation_etu.setText(((Etudiant) utilisateur).getFormation());
        	this.enseign_ens.setVisible(false);
        	this.raison_start.setVisible(false);
        	this.admin_ens.setVisible(false);
        	this.label_enseign_ens.setVisible(false);
        	this.label_raison_start.setVisible(false);
        	this.label_admin_ens.setVisible(false);
        }
        else if(utilisateur.getClass().getSimpleName().equals("Enseignant")) {
        	this.enseign_ens.setText(((Enseignant) utilisateur).getEnseignement());
        	if(((Enseignant) utilisateur).isAdministratif())
        		this.admin_ens.setSelected(true);
        	this.annee_etu.setVisible(false);
        	this.formation_etu.setVisible(false);
        	this.raison_start.setVisible(false);
        	this.label_annee_etu.setVisible(false);
        	this.label_formation_etu.setVisible(false);
        	this.label_raison_start.setVisible(false);
        }
        else if(utilisateur.getClass().getSimpleName().equals("Startup")) {
        	this.raison_start.setText(((Startup) utilisateur).getRaisonSociale());
        	this.annee_etu.setVisible(false);
        	this.formation_etu.setVisible(false);
        	this.enseign_ens.setVisible(false);
        	this.admin_ens.setVisible(false);
        	this.label_annee_etu.setVisible(false);
        	this.label_formation_etu.setVisible(false);
        	this.label_enseign_ens.setVisible(false);
        	this.label_admin_ens.setVisible(false);
        }
        	
    }
    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }

    /**
     * Méthode appelé lorsque l'utilisateur clique sur OK pour valider l'utilisateur
     */
    @FXML
    private void gestionOk() {
        if (isInputValid()) {
                  
           utilisateur.setNom(nom_util.getText());
           utilisateur.setPrenom(prenom_util.getText());
           utilisateur.setMail(mail_util.getText());
           utilisateur.setAdresse(adresse_util.getText());
           utilisateur.setNum_tel(tel_util.getText());
           
           if(utilisateur.getClass().getSimpleName().equals("Etudiant")) {
           	((Etudiant) utilisateur).setFormation(formation_etu.getText());
           	((Etudiant) utilisateur).setAnnee(annee_etu.getText());
           }
           else if(utilisateur.getClass().getSimpleName().equals("Enseignant")) {
            ((Enseignant) utilisateur).setEnseignement(enseign_ens.getText());
           	
	           if(this.admin_ens.isSelected())
	               ((Enseignant) utilisateur).setAdministratif(true);
	           else
	               ((Enseignant) utilisateur).setAdministratif(false);
           }
           else if(utilisateur.getClass().getSimpleName().equals("Startup"))
           	this.raison_start.setText(((Startup) utilisateur).getRaisonSociale());
           
    		okClick = true;
            dialogStage.close();
        }
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un emprunt
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
    
    /**
     * Méthode permettant d'effacer les saisies
     */
    @FXML
    private void gestionEffacer() {
    	if(utilisateur!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Effacer tous les champs");
		    alert.setHeaderText("Voulez-vous vraiment effacer tous les champs");
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
		        this.nom_util.setText(null);
		        this.prenom_util.setText(null);
		        this.adresse_util.setText(null);
		        this.tel_util.setText(null);
		        this.mail_util.setText(null);
		        this.formation_etu.setText(null);
		        this.annee_etu.setText(null);
		        this.enseign_ens.setText(null);
		        this.admin_ens.setSelected(false);
		        this.raison_start.setText(null);
		    } 
    	}

    }
    
    /**
     * Méthode permettant de gérer les saisies au clavier de l'utilisateurs
     *
     * @return Vrai si l'utilisation a bien saisie les champs, faux sinon
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (nom_util.getText()==null || nom_util.getText().isEmpty()) {
            errorMessage += "Veuillez rentrer un nom !\n";
        }
        else if(nom_util.getText().matches("[0-9]*")) {
            errorMessage += "Veuillez rentrer un nom valide!\n";
        }
        
        if (prenom_util.getText()==null ||prenom_util.getText().isEmpty()) {
            errorMessage += "Veuillez rentrer un prénom !\n";
        } 
        else if(prenom_util.getText().matches("[0-9]*")) {
            errorMessage += "Veuillez rentrer un prénom valide!\n";
        }
        
        if(nom_util.getText()==null || !tel_util.getText().matches("[0-9]*")) {
            errorMessage += "Veuillez rentrer un numéro de téléphone valide !\n";
        }
        else if(tel_util.getText().length()!=10)
        	errorMessage +="Veuillez rentrer un numéro à 10 chiffres\n";
               	

        if (errorMessage.length() == 0) return true;
        else {
            //Afficher un message d'erreur
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalides");
            alert.setHeaderText("Veuillez corriger les champs invalides");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
    
    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
       
    }
}
