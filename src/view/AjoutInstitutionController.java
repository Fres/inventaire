package view;

import controller.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.*;

/**
 * Cette classe est utilisée pour ajouter ou modifier une institution dans la fenetre AjoutInstitution
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class AjoutInstitutionController {

	/**
	 * Champs représentant le nom de l'institution
	 */
    @FXML
    private TextField nom_insti;
    
	/**
	 * Champs représentant l'adresse de l'institution
	 */
    @FXML
    private TextField adresse_insti;
        
    
    private Stage dialogStage;
    private Institution institution;
    private boolean okClick = false;
    
	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;

    /**
     * Méthode initialisant le contrôleur de la classe AjoutInstitutionController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'afficher les détails d'une institution
     *
     * @param institution L'institution
     */
    public void setInstitution(Institution institution) {
 
        this.institution = institution;

    	if(institution.getName()!=null) {
    	    nom_insti.setText(institution.getName());
    	}
    	if(institution.getAdresse()!=null) {
    	    adresse_insti.setText(institution.getAdresse());
    	}
  
    }

    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }

    /**
     * Méthode appelé lorsque l'utilisateur clique sur OK pour valider l'institution
     */
    @FXML
    private void gestionOk() {
        if (isInputValid()) {
        	
        	this.institution.setName(nom_insti.getText());
        	this.institution.setAdresse(adresse_insti.getText());
        		    
    		okClick = true;
            dialogStage.close();
        }
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'une institution
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
    
    /**
     * Méthode permettant d'effacer les saisies
     */
    @FXML
    private void gestionEffacer() {
        this.adresse_insti.setText(null);
        this.nom_insti .setText(null);
    }
    
    
    /**
     * Méthode permettant de gérer les saisies au clavier de l'utilisateurs
     *
     * @return Vrai si l'utilisation a bien saisie les champs, faux sinon
     */
    private boolean isInputValid() {
	    String errorMessage = "";
	
	    if (nom_insti.getText()==null || nom_insti.getText().isEmpty()) {
	        errorMessage += "Veuillez rentrer un nom !\n";
	    }
	    else if(nom_insti.getText().matches("[0-9]*")) {
	        errorMessage += "Veuillez rentrer un nom valide!\n";
	    }
	    
	    if (nom_insti.getText()==null ||adresse_insti.getText().isEmpty()) {
	        errorMessage += "Veuillez rentrer une adresse !\n";
	    } 
 	

        if (errorMessage.length() == 0) return true;
        else {
            //Afficher un message d'erreur
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalides");
            alert.setHeaderText("Veuillez corriger les champs invalides");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
    
    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;        
    }
}
