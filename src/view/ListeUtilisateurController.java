/**
 * 
 */
package view;


import java.util.Optional;
import java.util.function.Predicate;

import controller.MainApp;
import model.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;


/**
 * Cette classe est utilisée pour gérer les données dans le tableau des emprunts
 *
 * @author Frescinel BART et Yoan BOITEUX
 *
 */
public class ListeUtilisateurController {

	/**
	 * Table représentant la liste des utilisateur
	*/
	@FXML
    private TableView<Utilisateur> utilisateurTable;
	
	/**
	 * Colonne du tableau représentant le nom de l'utilisateur
	*/
    @FXML 
    private TableColumn<Utilisateur, String> nom_utilisateur;
    
	/**
	 * Colonne du tableau représentant le nom de l'utilisateur
	*/
    @FXML
    private TableColumn<Utilisateur, String> prenom_utilisateur;
    
	/**
	 * Colonne du tableau représentant la fonction de l'utilisateur
	*/
    @FXML
    private TableColumn<Utilisateur, String> fonction_utilisateur;
    
	/**
	 * Colonne du tableau représentant le numéro de téléphone de l'utilisateur
	*/
    @FXML
    private TableColumn<Utilisateur, String> numtel_utilisateur;
    
	/**
	 * Colonne du tableau représentant l'adresse de l'utilisateur
	*/
    @FXML
    private TableColumn<Utilisateur, String> adresse_utilisateur;
    
	/**
	 * Colonne du tableau représentant le mail de l'utilisateur
	*/
    @FXML
    private TableColumn<Utilisateur, String> mail_utilisateur;
    
	/**
	 * Colonne du tableau représentant l'année de formation de l'étudiant
	*/
    @FXML
    private TableColumn<Etudiant, String> annee_etudiant;
    
	/**
	 * Colonne du tableau représentant la formation de l'étudiant
	*/
    @FXML
    private TableColumn<Etudiant, String> formation_etudiant;
    
	/**
	 * Colonne du tableau représentant la matière de l'enseignant
	*/
    @FXML
    private TableColumn<Enseignant, String> matiere_enseignant;

	/**
	 * Colonne du tableau permettant de savoir si l'enseignant est un administratif
	*/
    @FXML
    private TableColumn<Enseignant, Boolean> admin_enseignant;
    
	/**
	 * Colonne du tableau représentant la raison sociale de la startup
	*/
    @FXML
    private TableColumn<Startup, String> raison_startup;
    
    /**
     * Barre de recherche
     */
    @FXML
    private TextField recherche;
    
	/**
	 * Element menu pour l'ajout d'un etudiant
	 */
	@FXML
	private MenuItem ajout_etudiant;
	
	/**
	 * Element menu pour l'ajout d'un enseignant
	 */
	@FXML
	private MenuItem ajout_enseignant;
	
	/**
	 * Element menu pour l'ajout d'une startup
	 */
	@FXML
	private MenuItem ajout_startup;
    
    /**
     * Bouton ok
     */
    @FXML
    private Button ok;
    
    /**
     * Bouton annuler
     */
    @FXML
    private Button annuler;
       
    /**
     * Liste des checkboxs
     */
    private ObservableList<CheckBox> selectedCheckBoxes = FXCollections.observableArrayList();
    
    /**
     * Checkbox etudiant
     */
    @FXML
    private CheckBox etudiant;
    
    /**
     * Checkbox enseignant
     */
    @FXML
    private CheckBox enseignant;
    
    /**
     * Checkbox startup
     */
    @FXML
    private CheckBox startup;

	/**
	 * Référence de l'application main
	*/
    private MainApp mainApp;
    
    private Stage dialogStage;
    private boolean okClick = false;

    /**
     * Le constructeur du contrôleur ListeUtilisateur
     * Le constructeur est appelé avant la méthode initialize
     */
    public ListeUtilisateurController() {
    }

    /**
     * Méthode initialisant le contrôleur de la classe ListeUtilisateurController. Cette méthode est appelé automatiquement
     * après le chargement du fichier fxml
     */
    @FXML
    private void initialize() {

        // Initialise la table des utilisateurs
    	
        nom_utilisateur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        prenom_utilisateur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrenom()));
        fonction_utilisateur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getClass().getSimpleName()));
        adresse_utilisateur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAdresse()));
        numtel_utilisateur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNum_tel()));
        mail_utilisateur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMail()));

        /*annee_etudiant.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getClass().getSimpleName().equals("Etudiant") ? cellData.getValue().getAnnee() : null));
        formation_etudiant.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getClass().getSimpleName().equals("Etudiant") ? cellData.getValue().getFormation(): null));
        matiere_enseignant.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getClass().getSimpleName().equals("Enseignant") ? cellData.getValue().getEnseignement(): null));
        admin_enseignant.setCellValueFactory(cellData -> new SimpleObjectProperty(cellData.getValue().getClass().getSimpleName().equals("Enseignant") ? cellData.getValue().isAdministratif(): null));
        raison_startup.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getClass().getSimpleName().equals("Startup") ? cellData.getValue().getRaisonSociale(): null));*/
      
        //Déclenche la recherche quand il y a une saisie au clavier
        this.recherche.setOnKeyPressed(e -> {recherche(e);});
        
        //Ajout des checkbox dans la liste
        selectedCheckBoxes.addAll(this.etudiant,this.enseignant,this.startup);

        //Déclenche la recherche quand il y a une checkbox a été selectionné
        this.etudiant.setOnAction(e -> {filtrecheckbox(e);});
        this.enseignant.setOnAction(e -> {filtrecheckbox(e);});
        this.startup.setOnAction(e -> {filtrecheckbox(e);});
                
    }
    
    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage Le stage a set
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Méthode permettant d'effectuer une recherche sur la table des utilisateurs selon la fonction de l'utilisateur
     * 
     *@param e Action déclenchée par la selection d'une checkbox
     */
    public void filtrecheckbox(ActionEvent e) {
		FilteredList<Utilisateur> filterData = new FilteredList<Utilisateur>(mainApp.getListeUtilisateurs());
		String newValue,Value="";
		if(selectedCheckBoxes.size()!=0) {
			for (int i = 0; i < selectedCheckBoxes.size(); i++) {
				if(selectedCheckBoxes.get(i).isSelected())
					Value += " "+selectedCheckBoxes.get(i).textProperty().getValue();
			}
		}

		if(!Value.isEmpty()) newValue = Value;
		else newValue=null;
		
		filterData.setPredicate((Predicate<? super Utilisateur>) utilisateur-> {
			if(newValue == null || newValue.isEmpty())
					return true;
			
			String lowerCaseFilter = newValue.toLowerCase();
	
			//Recherche selon le nom de l'utilisateur
			if(newValue.toLowerCase().indexOf(utilisateur.getClass().getSimpleName().toLowerCase()) != -1)
				return true;
								
			return false;
		});
		
		SortedList<Utilisateur> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.utilisateurTable.comparatorProperty());
		this.utilisateurTable.setItems(sortedData);
    }
    
    /**
     * Méthode permettant d'effectuer une recherche sur la table des utilisateurs
     * 
     *@param e Action déclenchée par la saisie du clavier
     */
	public void recherche(KeyEvent e)
	{
		FilteredList<Utilisateur> filterData = new FilteredList<Utilisateur>(mainApp.getListeUtilisateurs());
		this.recherche.textProperty().addListener((observableValue, olValue, newValue) ->{
			filterData.setPredicate((Predicate<? super Utilisateur>) utilisateur-> {
				if(newValue == null || newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(utilisateur.getNom().toLowerCase().contains(newValue.toLowerCase()))
					return true;
				if(utilisateur.getPrenom().toLowerCase().contains(newValue.toLowerCase()))
					return true;
				if(utilisateur.getClass().getSimpleName().toLowerCase().contains(newValue.toLowerCase()))
					return true;
				
				return false;
			});
		});
		
		SortedList<Utilisateur> sortedData = new SortedList<>(filterData);
		sortedData.comparatorProperty().bind(this.utilisateurTable.comparatorProperty());
		this.utilisateurTable.setItems(sortedData);
	}
	
    /**
     * Méthode retournant vrai si l'utilisateur a cliqué sur le bouton "Ok...", faux sinon
     * 
     * @return Vrai ou faux
     */
    public boolean estOkclique() {
        return okClick;
    }
    
    /**
     * Méthode appelée lorsque l'utilisateur clique sur OK pour valider l'utilisateur
     */
    @FXML
    private void gestionOk() {

    	int index_last = mainApp.getListeEmprunt().size() - 1;
        Utilisateur utilisateur = utilisateurTable.getSelectionModel().getSelectedItem();
        mainApp.getListeEmprunt().get(index_last).setUtilisateur(utilisateur);
        okClick = true;
        dialogStage.close();
    }

    /**
     * Méthode permettant d'annuler les saisies et fermer la fenêtre d'édition d'un utilisateur
     */
    @FXML
    private void gestionAnnuler() {
        dialogStage.close();
    }
	
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..." puis "Etudiant
	 * Elle ouvre une fenetre permettant d'ajouter un etudiant
	 */
	@FXML
	private void ajouterEtudiant() {
	    Etudiant etu = new Etudiant(null,null,null,null,null,null,null);
	    mainApp.getListeUtilisateurs().add(etu);
    	int index_last = mainApp.getListeUtilisateurs().size() - 1;
	    boolean okClick = mainApp.interfaceGestionUtilisateur(mainApp.getListeUtilisateurs().get(index_last));
	    if (okClick)
	        mainApp.getListeUtilisateurs().set(index_last, mainApp.getListeUtilisateurs().get(index_last));
	    else
	    	mainApp.getListeUtilisateurs().remove(index_last);
	}
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..." puis "Etudiant
	 * Elle ouvre une fenetre permettant d'ajouter un etudiant
	 */
	@FXML
	private void ajouterEnseignant() {
	    Enseignant ens = new Enseignant(null,null,null,null,null,null,false);
	    mainApp.getListeUtilisateurs().add(ens);
    	int index_last = mainApp.getListeUtilisateurs().size() - 1;
	    boolean okClick = mainApp.interfaceGestionUtilisateur(mainApp.getListeUtilisateurs().get(index_last));
	    if (okClick)
	        mainApp.getListeUtilisateurs().set(index_last, mainApp.getListeUtilisateurs().get(index_last));
	    else
	    	mainApp.getListeUtilisateurs().remove(index_last);
	}
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Ajouter..." puis "Etudiant
	 * Elle ouvre une fenetre permettant d'ajouter un etudiant
	 */
	@FXML
	private void ajouterStarUp() {
	    Startup star = new Startup(null,null,null,null,null,null);
	    mainApp.getListeUtilisateurs().add(star);
    	int index_last = mainApp.getListeUtilisateurs().size() - 1;
	    boolean okClick = mainApp.interfaceGestionUtilisateur(mainApp.getListeUtilisateurs().get(index_last));
	    if (okClick)
	        mainApp.getListeUtilisateurs().set(index_last, mainApp.getListeUtilisateurs().get(index_last));
	    else
	    	mainApp.getListeUtilisateurs().remove(index_last);
	}

	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Modifier..."
	 * Elle ouvre une fenetre permettant d'ajouter un utilisateur
	 */
	@FXML
	private void modifierUtilisateur() {
	    Utilisateur utilisateur = utilisateurTable.getSelectionModel().getSelectedItem();
	    if (utilisateur != null) {
	        boolean okClicked = mainApp.interfaceGestionUtilisateur(utilisateur);
	        if (okClicked) {
	        	int indexSelect = utilisateurTable.getSelectionModel().getSelectedIndex();
	        	utilisateurTable.getItems().set(indexSelect, utilisateur);
	        }

	    } else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas d'utilisateur selectionné");
	        alert.setContentText("Veuillez selectionner un utilisateur dans la table.");
	        alert.showAndWait();
	        }
	}
	
	/**
	 * Méthode appelé lorsque l'utilisateur clique sur le bouton "Supprimer..."
	 * Elle ouvre une fenetre de confirmation
	 */
	@FXML
	private void supprimerUtilisateur() {
        
	    Utilisateur utilisateur= utilisateurTable.getSelectionModel().getSelectedItem();
	    if(utilisateur!=null) {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Supprimer l'utilisateur");
		    alert.setHeaderText("Confirmez-vous la suppression de l'utilisateur ?");
		    alert.setContentText(utilisateur.getNom()+" "+utilisateur.getPrenom());
		    Optional<ButtonType> option = alert.showAndWait();
		    
		    if (option.get() == ButtonType.OK) {
	        	int indexSelect = utilisateurTable.getSelectionModel().getSelectedIndex();
	        	this.supprimerEmprunts(utilisateur);
		        mainApp.getListeUtilisateurs().remove(indexSelect);
		    }    	
	    }
	    else {
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("Pas de selection");
	        alert.setHeaderText("Pas d'utilisateur selectionné");
	        alert.setContentText("Veuillez selectionner un utilisateur dans la table.");
	        alert.showAndWait();
	        }
	}
	
	/**
	 * Méthode permettant de supprimer les emprunts ayant l'utilisateur en paramètre
	 * 
	 * @param util L'utilisateur dont l'emprunt va être supprimé
	 */
	public void supprimerEmprunts(Utilisateur util) {
		int index=0;
    	while(index<mainApp.getListeEmprunt().size()) {
    		if(util.equals(mainApp.getListeEmprunt().get(index).getUtilisateur())) {
    			mainApp.getListeEmprunt().get(index).getMat().retirerEmprunter(mainApp.getListeEmprunt().get(index));
				mainApp.getListeEmprunt().remove(index);
    		}
    		index++;
    	}
	}

    /**
     * Méthode permettant de référencer l'application main
     * 
     * @param mainApp Application principale
     * @param disable Vrai s'il ne faut pas rendre visible le bouton ok , faux sinon
     */
    public void setMainApp(MainApp mainApp,boolean disable) {
        this.mainApp = mainApp;
        
        if(disable) {
        	this.ok.setVisible(false);
        	this.annuler.setText("Fermer");
        }

        // Ajout de la liste de données observable dans la table
        
        utilisateurTable.setItems(mainApp.getListeUtilisateurs());
    }

}
